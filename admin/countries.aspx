﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="countries.aspx.cs" Inherits="admin_countries" %>

<%@ Register Src="~/controls/admin-nav.ascx" TagName="anav" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:anav runat="server" ID="nav" />
    <div class="clearfix"></div>

    <p>
        &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vSummary" />
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title">Add New Country</h3>
            </div>
            <div class="panel-body">
                <asp:Label ID="lblMsg" runat="server"></asp:Label>
                <div class="form-horizontal">
                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="selectbasic">
                            Country Name:</label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtCountryName" runat="server" Width="160" CssClass="form-control"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter a country name" ControlToValidate="txtCountryName" ValidationGroup="new"></asp:RequiredFieldValidator>

                        </div>
                    </div>

                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="selectbasic">
                            Image:
                        </label>
                        <div class="col-md-4">
                            <asp:FileUpload ID="fuImage" runat="server" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                runat="server" ErrorMessage="Select a file." ControlToValidate="fuImage" ValidationGroup="new"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="selectbasic">
                            Status:
                        </label>
                        <div class="col-md-4">
                            <asp:DropDownList ID="dlStatus" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <!-- Select Basic -->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="selectbasic">
                            Sort Order:
                        </label>
                        <div class="col-md-4">
                            <asp:TextBox ID="txtSortOrder" runat="server" Width="40" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="singlebutton"></label>
                        <div class="col-md-4">

                            <asp:Button ID="btnInsert" runat="server" Text="Add New"
                                OnClick="btnInsert_Click" CssClass="btn btn-primary" ValidationGroup="new" />
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Existing Countriesries</h3>
            </div>
            <div class="panel-body">


                <div class="col-md-12 ">
                    <hr class="divider" />
                    <div class="col-md-10">
                        <div class="form-inline">
                            Keyword: 
                              <asp:TextBox ID="txtKeyword" runat="server" MaxLength="70" Width="170" CssClass="form-control input-md"></asp:TextBox>
                            <asp:Button ID="btnSearch" runat="server" Text="Search"
                                CssClass="btn btn-primary" ValidationGroup="search" />
                            <asp:Label ID="lblDelmsg" runat="server" CssClass="label label-danger"></asp:Label>
                            <input id="select_all" type="checkbox" />
                            Select All
                                       <p></p>
                        </div>

                    </div>
                    <div class="col-md-2 right">

                       <%-- <asp:Button ID="btnDelete" runat="server" CausesValidation="false" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />
                       --%> <asp:Button ID="Button1" runat="server" CausesValidation="false" OnClick="btnDelete_Click" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />
                        
                    </div>
                </div>


                <asp:GridView ID="GridView2" runat="server" DataKeyNames="cid" CssClass="tablestyle" DataSourceID="dsCountries" AutoGenerateColumns="false" OnRowDataBound="GridView2_RowDataBound">
                    <AlternatingRowStyle CssClass="altrowstyle" Width="99%" />
                    <HeaderStyle CssClass="headerstyle" />
                    <RowStyle CssClass="rowstyle" />
                    <FooterStyle CssClass="footerstyle" />
                    <Columns>
                        <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="~/images/EditContact.gif" UpdateImageUrl="~/images/scr_check_10x10.gif" CancelImageUrl="~/images/scr_x_10x10.gif" HeaderText="Edit" ItemStyle-HorizontalAlign="Center" ValidationGroup="edit" />


                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:ImageButton ID="DeleteButton" runat="server" ImageUrl="~/images/cross.gif" CommandName="Delete"
                                    OnClientClick="return confirm('Are you sure you want to delete this?');" AlternateText="Delete" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="#" SortExpression="cid">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("cid") %>'></asp:Label>
                                <%--<%#Eval("cid") %>--%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <%#Eval("cid") %>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Country Name" SortExpression="country_Name">
                            <ItemTemplate>
                                <%# Eval("country_name")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLoginTitle" runat="server" Text='<%# Bind("country_name")%>' MaxLength="30" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLoginTitle" ErrorMessage="Enter country name" Display="Dynamic" ValidationGroup="edit">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sorting" SortExpression="sort_order">
                            <ItemTemplate>
                                <%# Eval("sort_order")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtSortOrder" runat="server" Text='<%# Bind("sort_order")%>' MaxLength="10" Width="40" />

                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Image Name" SortExpression="image">
                            <ItemTemplate>
                                <%# Eval("image")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <%#Eval("image") %>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status" SortExpression="status">
                            <ItemTemplate>
                                <%#Eval("status") %>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="dlStatus" runat="server" SelectedValue='<%#Bind("status") %>'>
                                    <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                                </asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkDelete"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>


                <asp:SqlDataSource ID="dsCountries" runat="server"
                    ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                    SelectCommand="select * from cms_countries" 
                    FilterExpression="country_name like '%{0}%'"
                    UpdateCommand="update cms_countries set country_name=@country_name, status=@status, sort_order=@sort_order where cid=@cid"
                    InsertCommand="insert into cms_countries (country_name, status, image, sort_order) values(@country_name,@status, @image,@sort_order)"
                    DeleteCommand="DELETE FROM cms_countries where cid = @cid">
                    <UpdateParameters>
                        <asp:Parameter Name="cid" Type="Object" />
                        <asp:Parameter Name="country_name" Type="String" />
                        <asp:Parameter Name="status" Type="String" />
                        <asp:Parameter Name="sort_order" Type="Int32" />
                    </UpdateParameters>

                    <InsertParameters>

                        <asp:ControlParameter ControlID="txtCountryName" Name="country_name" Type="String" />
                        <asp:ControlParameter ControlID="dlStatus" Name="status" PropertyName="selectedValue" Type="String" />
                        <asp:ControlParameter ControlID="txtSortOrder" Name="sort_order" Type="Int32" DefaultValue="0" />
                    </InsertParameters>
                    
                    <filterparameters>
                    <asp:ControlParameter ControlID="txtKeyword" Type="String" Name="keyword" />
                </filterparameters>
                </asp:SqlDataSource>
            </div>
        </div>

        <script src="../js/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="../js/quicksearch.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('.search_textbox').each(function (i) {
                    $(this).quicksearch("[id*=GridView1] tr:not(:has(th))", {
                        'testQuery': function (query, txt, row) {
                            return $(row).children(":eq(" + i + ")").text().toLowerCase().indexOf(query[0].toLowerCase()) != -1;
                        }
                    });
                });
            });


            $(document).ready(function () {
                $('#select_all').change(function () {
                    // alert("I am working")
                    var checkboxes = $(this).closest('form').find(':checkbox');
                    if ($(this).is(':checked')) {
                        checkboxes.prop('checked', true);
                    } else {
                        checkboxes.prop('checked', false);
                    }
                });

            });
        </script>
</asp:Content>

