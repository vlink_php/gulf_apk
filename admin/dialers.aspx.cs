﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class admin_dialers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["did"] == null)
        {
            FormView1.ChangeMode(FormViewMode.Insert);


            CheckBoxList chkCountry = (CheckBoxList)FormView1.FindControl("chkCountry");
            //collect and format the checkbox list values and set to string variable
            string st = GetStringFromCheckBoxList(chkCountry);


            //assign update parameter values here to update database
            dsDialers.InsertParameters.Add("country", st);




        }
        else
        {
            FormView1.ChangeMode(FormViewMode.Edit);
            CheckBoxList chkCountry = (CheckBoxList)FormView1.FindControl("chkCountry");
            //collect and format the checkbox list values and set to string variable
            string st = GetStringFromCheckBoxList(chkCountry);
            dsDialers.UpdateParameters.Add("country", st);

        }




    }

    protected void btnInsert_Click(object sender, EventArgs e)
    {

        FileUpload fuImage = (FileUpload)FormView1.FindControl("fuImage");
        string fname = fuImage.PostedFile.FileName;
        string UpPath = Server.MapPath(@"~/images/");

        string fn = System.IO.Path.GetFileName(fname);


        dsDialers.InsertParameters.Add("image", fname);
        dsDialers.Insert();

        //save the file
        fuImage.PostedFile.SaveAs(UpPath + fn);
        lblMsg.Text = "Dialer added!";
        GridView2.DataBind();

    }

    public static string GetStringFromCheckBoxList(CheckBoxList chk)
    {

        string strTemp = string.Empty;

        foreach (ListItem lst in chk.Items)
        {
            if (lst.Selected)
            {
                strTemp = strTemp + lst.Value + ",";
            }
        }

        strTemp = strTemp.Trim(new char[] { ',' });
        return strTemp;

    }

    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {


        GridViewRow row = GridView2.Rows[e.RowIndex];

        //then u can find control of checkbox

        CheckBoxList cblCountry = (CheckBoxList)row.FindControl("chkCountry");



    }

    protected void FormView1_inserting(object sender, FormViewInsertEventArgs e)
    {

        FileUpload fuImage = (FileUpload)FormView1.FindControl("fuImage");
        string fname = fuImage.PostedFile.FileName;
        string UpPath = Server.MapPath(@"~/images/");

        string fn = System.IO.Path.GetFileName(fname);


        dsDialers.InsertParameters.Add("image", fname);
        //  dsDialers.Insert();
        GridView2.DataBind();
        //save the file
        fuImage.PostedFile.SaveAs(UpPath + fn);
        lblMsg.Text = "Dialer added!";
    }

    protected void FormView1_Databound(object sender, EventArgs e)
    {
        try
        {
            if (FormView1.CurrentMode == FormViewMode.Edit)
            {
                CheckBoxList chkList = (CheckBoxList)FormView1.FindControl("chkCountry");
                Label lblCountry = (Label)FormView1.FindControl("lblCountry");

                string cname = lblCountry.Text;
                string[] cval = cname.Split(',');
                if (cval.Length > 0)
                {
                    for (int i = 0; i <= chkList.Items.Count - 1; i++)
                    {
                        if (cval.Contains(chkList.Items[i].Value))
                        {
                            chkList.Items[i].Selected = true;
                        }
                    }

                }
            }
        }
        catch (Exception)
        {


        }

    }

    protected void FormView1_ItemUpdated(object sender, FormViewUpdatedEventArgs e)
    {
        lblMsg.Text = "The record has been updated";
        lblMsg.ForeColor = System.Drawing.Color.Red;

        GridView2.DataBind();
    }

    protected void GridView2_OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridViewRow row = GridView2.Rows[e.NewEditIndex];
        //bool IsChecked = ((CheckBox)(row.Cells[3].FindControl("chkbox"))).Checked;
        Label lblCountry = (Label)(row.Cells[4].FindControl("lblCountry"));
        CheckBoxList cblCountry = (CheckBoxList)(row.Cells[4].FindControl("chkCountry"));
        string strCountry = lblCountry.Text;
        string[] str = strCountry.Split(',');

        if (str.Length > 0)
        {
            //
            for (int i = 0; i <= cblCountry.Items.Count - 1; i++)
            {
                if (str.Contains(cblCountry.Items[i].Value))
                {
                    cblCountry.Items[i].Selected = true;
                }
            }
        }
    }



    protected void GridView2_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        GridView gridView = (GridView)sender;

        if (gridView.SortExpression.Length > 0)
        {
            int cellIndex = -1;
            foreach (DataControlField field in gridView.Columns)
            {
                if (field.SortExpression == gridView.SortExpression)
                {
                    cellIndex = gridView.Columns.IndexOf(field);
                    break; // TODO: might not be correct. Was : Exit For
                }
            }

            if (cellIndex > -1)
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //  this is a header row,
                    //  set the sort style
                    e.Row.Cells[cellIndex].CssClass += (gridView.SortDirection == SortDirection.Ascending ? " sortascheader" : " sortdescheader");
                }
                else if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //  this is an alternating row
                    e.Row.Cells[cellIndex].CssClass += (e.Row.RowIndex % 2 == 0 ? " sortaltrow" : "sortrow");


                }
            }
        }


        string usr = HttpContext.Current.User.Identity.Name;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (usr != null & usr != "admin")
            {
                e.Row.Cells[10].Enabled = false;
                e.Row.Cells[10].Text = "";

            }
        }
    }
    private int count = 0;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter adp;
    SqlDataReader rd;
    DataSet ds;
    string query;
    public void dbcon()
    {

        string connn = (System.Configuration.ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString());
        con = new SqlConnection(connn);
        con.Open();

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow Grow in GridView2.Rows)
        {
            CheckBox chselect = (CheckBox)Grow.FindControl("chkDelete");
            Label dtype = (Label)Grow.FindControl("Label3");


            if (chselect.Checked)
            {

                dbcon();
                query = "delete from cms_dialer_type where dtype  ='" + dtype.Text + "'";

                cmd = new SqlCommand(query, con);
                count += 1;
                cmd.ExecuteNonQuery();
                // ltlMsg.Text = "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'><i class='icon-remove'></i></button><strong> The record has been approved!</strong><br></div>";
                GridView2.DataBind();

                lblDelmsg.Text = "Total " + count + " records have been deleted!";
            }
        }
    }
    protected void FormView1_ItemInserted(object sender, FormViewInsertedEventArgs e)
    {
        GridView2.DataBind();
        Response.Redirect("dialers.aspx");
        lblMsg.Text = "Dialer added!";
    }
}