﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Globalization;

public partial class MasterPage : System.Web.UI.MasterPage
{

    
    
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!HttpContext.Current.User.Identity.IsAuthenticated)
        {
            admin.Visible=false;
            login.Visible = false;

        }

        

        string filename = Path.GetFileName(Request.Path);
   

        if (filename == "default.aspx" || filename==null)
        {
            //btnBack.Visible = false;
            back.Visible = false;
        }

        if (!Page.IsPostBack)
        {
            Int32 intNumber;
            //display online visitors
            intNumber=(Int32) Application["ActiveUsers"];
            txtOnlineUsers.Text = intNumber.ToString();
        }
    }

   

    
}
