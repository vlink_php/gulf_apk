﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class admin_download_links : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

        }
        string usr = HttpContext.Current.User.Identity.Name;


        if (usr != null & usr != "admin")
        {
            btnDelete.Enabled = false;
            btnDelete.Attributes.Add("class", "disabled");

        }
        
    }


    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        // Iterates through the rows of the GridView control
        GridViewRow row = GridView1.FooterRow;

        string txtOpc = ((TextBox)row.FindControl("txtOpc")).Text;
        string txtDialerTitle = ((TextBox)row.FindControl("txtDialerTitle")).Text;
        string txtDownloadLink = ((TextBox)row.FindControl("txtDownloadLink")).Text;
        string txtStatus = ((DropDownList)row.FindControl("dlStatus")).SelectedValue;
        string dlCountries = ((DropDownList)row.FindControl("dlCountries")).SelectedValue;
        string txtDialerType = ((DropDownList)row.FindControl("dlDialerType")).SelectedValue;
        string txtDeviceType = ((DropDownList)row.FindControl("dlDeviceType")).SelectedValue;

        dsNews.InsertParameters.Add("download_link", txtDownloadLink);
        dsNews.InsertParameters.Add("status", txtStatus);
        dsNews.InsertParameters.Add("cid", dlCountries);
        dsNews.InsertParameters.Add("dialer_type", txtDialerType);
        dsNews.InsertParameters.Add("device_type", txtDeviceType);
        dsNews.InsertParameters.Add("dailer_title", txtDialerTitle);
        dsNews.InsertParameters.Add("opc", txtOpc);

        try
        {
            dsNews.Insert();
            lblMsg.Text = txtDownloadLink + "( Device: " + txtDeviceType + ")" + "- added to the list";
            GridView1.DataBind();
        }
        catch (Exception ex)
        {
            lblMsg.Text = ex.ToString();
        }
    }

    protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        GridView gridView = (GridView)sender;

        if (gridView.SortExpression.Length > 0)
        {
            int cellIndex = -1;
            foreach (DataControlField field in gridView.Columns)
            {
                if (field.SortExpression == gridView.SortExpression)
                {
                    cellIndex = gridView.Columns.IndexOf(field);
                    break; // TODO: might not be correct. Was : Exit For
                }
            }

            if (cellIndex > -1)
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //  this is a header row,
                    //  set the sort style
                    e.Row.Cells[cellIndex].CssClass += (gridView.SortDirection == SortDirection.Ascending ? " sortascheader" : " sortdescheader");
                }
                else if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //  this is an alternating row
                    e.Row.Cells[cellIndex].CssClass += (e.Row.RowIndex % 2 == 0 ? " sortaltrow" : "sortrow");


                }
            }
        }
    }

    protected void btnInsert_Click(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            dsNews.Insert();
            lblMsg.Text = "News has been posted successfully!";
            //bind the gridview after inserted
            GridView1.DataBind();

            txtDownloadLink.Text = "";
            txtTitle.Text = "";
        }
    }

    private int count = 0;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter adp;
    SqlDataReader rd;
    DataSet ds;
    string query;

    public void dbcon()
    {

        string connn = (System.Configuration.ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString());
        con = new SqlConnection(connn);
        con.Open();

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow Grow in GridView1.Rows)
        {
            CheckBox chselect = (CheckBox)Grow.FindControl("chkDelete");
            Label dnId = (Label)Grow.FindControl("Label3");


            if (chselect.Checked)
            {

                dbcon();
                query = "delete from cms_dialers where dnId  ='" + dnId.Text + "'";

                cmd = new SqlCommand(query, con);
                count += 1;
                cmd.ExecuteNonQuery();
                // ltlMsg.Text = "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'><i class='icon-remove'></i></button><strong> The record has been approved!</strong><br></div>";
                GridView1.DataBind();

                lblDelmsg.Text = "Total " + count + " records have been deleted!";
            }
        }
    }


    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        if (GridView1.Rows.Count > 0)
        {
            GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
            for (int i = 1; i < GridView1.Columns.Count; i++)
            {
                TableHeaderCell cell = new TableHeaderCell();
                TextBox txtSearch = new TextBox();
                txtSearch.Attributes["placeholder"] = GridView1.Columns[i].HeaderText;
                txtSearch.CssClass = "search_textbox";
                cell.Controls.Add(txtSearch);
                row.Controls.Add(cell);
            }
            GridView1.HeaderRow.Parent.Controls.AddAt(1, row);
        }
    }

        
        
}