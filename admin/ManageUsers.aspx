﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="ManageUsers.aspx.vb" Inherits="Administration_ManageUsers" title="Manage System Users" %>
<%@ Register src="~/controls/GridViewPager.ascx" tagname="GridViewPager" tagprefix="asp" %>
<%@ Register src="~/controls/admin-nav.ascx" tagname="anav" tagprefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:anav runat="server" ID="nav" />

    <h2>        Manage Users</h2>
    <p>To create a new user fill in the following user information.</p>
   <fieldset><legend><h3>Create user</h3></legend>
       <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" ContinueDestinationPageUrl="~/admin/ManageUsers.aspx" CssClass="form-horizontal" NavigationStyle-Wrap="false">
           <WizardSteps>
               <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                   <CustomNavigationTemplate>
                       
                    <p>            <asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" CssClass="btn btn-primary" 
                                       Text="Create User" ValidationGroup="CreateUserWizard1" />  </p>   
                              
                   </CustomNavigationTemplate>
               </asp:CreateUserWizardStep>
               
<asp:CompleteWizardStep runat="server"></asp:CompleteWizardStep>
               
           </WizardSteps>
           
       </asp:CreateUserWizard>
   
   </fieldset>
        
  


    <p>Displaying all system users who got permission to access this system.</p>
        <p><asp:Label ID="ActionStatus" runat="server"  Visible="false" /></p>
       

    <asp:GridView ID="MemberList" runat="server" AutoGenerateColumns="False" CssClass="tablestyle"
            DataKeyNames="Username" DataSourceID="MembershipUserListDataSource" Width="98%" >
             <AlternatingRowStyle CssClass="altrowstyle" Width="99%" />
            <HeaderStyle CssClass="headerstyle" />
             <RowStyle CssClass="rowstyle" />
             <FooterStyle CssClass="footerstyle" />
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Delete" ImageUrl="../images/cross.gif"  
                            OnClientClick="return confirm('This will permanently delete this user. Are you sure you want to do this?');"
                            Text="Delete" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:HyperLinkField DataNavigateUrlFields="UserName" 
                    DataNavigateUrlFormatString="UserInformation.aspx?user={0}" Text="Manage" />
                <asp:BoundField DataField="UserName" HeaderText="UserName" ReadOnly="True" SortExpression="UserName" />
                <asp:BoundField DataField="CreationDate" HeaderText="Date Created" ReadOnly="True"
                    SortExpression="CreationDate" DataFormatString="{0:d}" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                <asp:CheckBoxField DataField="IsApproved" HeaderText="Approved?" />
                <asp:CheckBoxField DataField="IsLockedOut" HeaderText="Locked Out?" />
                <asp:CheckBoxField DataField="IsOnline" HeaderText="Online?" />
                <asp:BoundField DataField="Comment" HeaderText="Comment" />
            </Columns>
            <PagerStyle CssClass="footer" />        
                <SelectedRowStyle CssClass="selected" />
                <PagerTemplate>
                    <asp:GridViewPager ID="GridViewPager1" runat="server" />
                </PagerTemplate>
                <EmptyDataTemplate>
                    There are currently no items in to display.
                </EmptyDataTemplate>
        </asp:GridView>
        <asp:ObjectDataSource ID="MembershipUserListDataSource" runat="server" DeleteMethod="DeleteUser"
            SelectMethod="GetAllUsers" TypeName="System.Web.Security.Membership">
            <DeleteParameters>
                <asp:ControlParameter ControlID="MemberList" Name="username" PropertyName="SelectedValue" />
                <asp:Parameter Name="deleteAllRelatedData" DefaultValue="True" />
            </DeleteParameters>
        </asp:ObjectDataSource>
  
</asp:Content>

