﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="contact.aspx.vb" Inherits="contact" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<h2>Contact Us</h2>
<asp:Label ID="lblAdd" runat="server"  />

    <p>For any enquiry please fill up the details and submit to us.</p>
    
    <table cellpadding="3" class="form-horizontal" >
    <tr><td colspan="2">
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-error"  />
            
</td></tr>
<tr><td><span class="mandatory">*</span> Your Name: </td><td> <asp:TextBox ID="txtName" runat="server" Width="200px" />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Pleae enter your name." ControlToValidate="txtName" SetFocusOnError="true">*</asp:RequiredFieldValidator>
</td></tr>
<tr><td><span class="mandatory">*</span> Email Address: </td><td><asp:TextBox ID="txtEmail" runat="server"  Width="200px"  />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Pleae enter your Email Address." ControlToValidate="txtEmail" SetFocusOnError="true">*</asp:RequiredFieldValidator>
 
<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
        ErrorMessage="Enter a valid email address." ControlToValidate="txtEmail" 
        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" >*</asp:RegularExpressionValidator>
</td></tr>
<tr><td>Subject:</td><td><asp:TextBox ID="txtSubject" runat="server"  Width="200px" /></td></tr>


<tr><td valign="top"><span class="mandatory">*</span> Your query:</td><td><asp:TextBox ID="txtQuery" runat="server" TextMode="MultiLine" Rows="6" Width="300px" CssClass="inputbox"/> <br />
    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Type web description." ControlToValidate="txtQuery" Display="Dynamic" Text="*" />
</td></tr>
        
<tr><td>Enter code here:</td><td><asp:TextBox ID="txtSecurity" runat="server" Width="50px" /></td></tr>
<tr><td>Security Code:</td><td><img alt="" src="captcha.aspx" /><br />
</td></tr>

<tr><td></td><td><asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary" />
     
    
    
    </td></tr>
</table>


</asp:Content>

