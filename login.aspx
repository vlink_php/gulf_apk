﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="col-md-12 nopadding">
<p> </p>
                <div class="col-md-1">
                </div>
                <div class="col-md-11 nopadding">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                           Please sign in</h3>
                    </div>
                    <div class="panel-body">

                        
    <div class="panel panel-info"> Please note you need to login to access to the admin panel.</div>

    <asp:Login ID="Login1" runat="server" DestinationPageUrl="~/Default.aspx" >
        <LayoutTemplate>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="alert alert-error"
                ValidationGroup="Login1" />
           <div style="color:Red;">  <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal></div>

           <div class="form-group "> 
            <div class="input-group form-inline">
                Username:  <asp:TextBox ID="UserName" runat="server" CssClass="form-control" placeholder="Enter Username" />
                    
                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                    ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>
            </div>
            <div class="input-group form-inline">
            Password: <asp:TextBox ID="Password" runat="server" TextMode="Password" CssClass="form-control" placeholder="Enter Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                            ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1">*</asp:RequiredFieldValidator>

            </div>
            <div class="input-group form-inline">
            

                        <p>
                            
                            </p>
            </div>

            <div class="input-group form-inline">
            <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" CssClass="btn btn-primary"
                            ValidationGroup="Login1" />
            </div>
            </div>
            
                  
        </LayoutTemplate>
    </asp:Login>


    </div>
                    </div>
     </div>

                </div>
</asp:Content>


