﻿<%@ Control Language="C#" ClassName="question" %>

<script runat="server">

    public string  q_num
    {
        get
        {
            return q_num;
        }
        set
        {
            dsQuestion.SelectParameters.Add("q_num", value);
        }


    }

</script>
<asp:Repeater ID="Repeater1" runat="server" DataSourceID="dsQuestion">
<ItemTemplate>
 <%# Eval("description") %>
</ItemTemplate>
</asp:Repeater>

<asp:SqlDataSource ID="dsQuestion" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
    SelectCommand="SELECT description FROM assessment_questions WHERE ([q_num] = @q_num)">
</asp:SqlDataSource>
