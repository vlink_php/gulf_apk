﻿Partial Class Administration_UserInformation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack Then
            'Roles.CreateRole("admin")

            If HttpContext.Current.User.IsInRole("admin") = False Then
               Response.Redirect("~/admin/no-access.aspx")

            Else

                ' Bind the users and roles
                BindUsersToUserList()
                BindRolesToList()
                ' Check the selected user's roles
                CheckRolesForSelectedUser()

                ' If querystring value is missing, send the user to ManageUsers.aspx
                Dim userName As String = Request.QueryString("user")
                If String.IsNullOrEmpty(userName) Then
                    Response.Redirect("ManageUsers.aspx")
                End If

                ' Get information about this user
                Dim usr As MembershipUser = Membership.GetUser(userName)
                If usr Is Nothing Then
                    Response.Redirect("ManageUsers.aspx")
                End If

                UserNameLabel.Text = usr.UserName
                IsApproved.Checked = usr.IsApproved

                If usr.LastLockoutDate.Year < 2000 Then
                    LastLockoutDateLabel.Text = String.Empty
                Else
                    LastLockoutDateLabel.Text = usr.LastLockoutDate.ToShortDateString()

                    UnlockUserButton.Enabled = usr.IsLockedOut
                End If

                Me.UnlockUserButton.Attributes.Add("onclick", _
                             "return confirm('Are you sure you want to Unlock?');")
            End If

        End If
    End Sub

    Protected Sub IsApproved_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles IsApproved.CheckedChanged
        'Toggle the user's approved status
        Dim userName As String = Request.QueryString("user")
        Dim usr As MembershipUser = Membership.GetUser(userName)

        usr.IsApproved = IsApproved.Checked
        Membership.UpdateUser(usr)

        StatusMessage.Text = "The user's approved status has been updated."
    End Sub

    Protected Sub UnlockUserButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles UnlockUserButton.Click
        'Unlock the user account
        Dim userName As String = Request.QueryString("user")
        Dim usr As MembershipUser = Membership.GetUser(userName)

        usr.UnlockUser()

        UnlockUserButton.Enabled = False
        StatusMessage.Text = "The user account has been unlocked."
    End Sub

    


    Protected Sub btnChangePwd_Click(sender As Object, e As System.EventArgs) Handles btnChangePwd.Click
        Dim userName As String = Request.QueryString("user")
        Dim usr As MembershipUser = Membership.GetUser(userName)

        Try
            usr.ChangePassword(usr.ResetPassword(), txtNewPassword.Text.ToString)
            StatusMessage.Text = "Password has been changed"
        Catch ex As Exception
            StatusMessage.Text = ex.ToString()

        End Try



    End Sub

    Private Sub BindRolesToList()
        ' Get all of the roles
        Dim roleNames() As String = Roles.GetAllRoles()
        UsersRoleList.DataSource = roleNames
        UsersRoleList.DataBind()

        RoleList.DataSource = roleNames
        RoleList.DataBind()
    End Sub

#Region "'By User' Interface-Specific Methods"
    Private Sub BindUsersToUserList()
        ' Get all of the user accounts
        Dim users As MembershipUserCollection = Membership.GetAllUsers()
        UserList.DataSource = users
        UserList.DataBind()
    End Sub

    Protected Sub UserList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles UserList.SelectedIndexChanged
        CheckRolesForSelectedUser()
    End Sub

    Private Sub CheckRolesForSelectedUser()
        ' Determine what roles the selected user belongs to
        Dim selectedUserName As String = UserList.SelectedValue
        Dim selectedUsersRoles() As String = Roles.GetRolesForUser(selectedUserName)

        ' Loop through the Repeater's Items and check or uncheck the checkbox as needed
        For Each ri As RepeaterItem In UsersRoleList.Items
            ' Programmatically reference the CheckBox
            Dim RoleCheckBox As CheckBox = CType(ri.FindControl("RoleCheckBox"), CheckBox)

            ' See if RoleCheckBox.Text is in selectedUsersRoles
            If Linq.Enumerable.Contains(Of String)(selectedUsersRoles, RoleCheckBox.Text) Then
                RoleCheckBox.Checked = True
            Else
                RoleCheckBox.Checked = False
            End If
        Next
    End Sub

    Protected Sub RoleCheckBox_CheckChanged(ByVal sender As Object, ByVal e As EventArgs)
        ' Reference the CheckBox that raised this event
        Dim RoleCheckBox As CheckBox = CType(sender, CheckBox)

        ' Get the currently selected user and role
        Dim selectedUserName As String = UserList.SelectedValue
        Dim roleName As String = RoleCheckBox.Text

        ' Determine if we need to add or remove the user from this role
        If RoleCheckBox.Checked Then
            ' Add the user to the role
            Roles.AddUserToRole(selectedUserName, roleName)

            ' Display a status message
            ActionStatus.Visible = True
            ActionStatus.Text = String.Format("User {0} was added to role {1}.", selectedUserName, roleName)
        Else
            ' Remove the user from the role
            Roles.RemoveUserFromRole(selectedUserName, roleName)

            ' Display a status message
            ActionStatus.Visible = True
            ActionStatus.Text = String.Format("User {0} was removed from role {1}.", selectedUserName, roleName)
        End If

        ' Refresh the "by role" interface
        DisplayUsersBelongingToRole()
    End Sub
#End Region

#Region "'By Role' Interface-Specific Methods"
    Protected Sub RoleList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RoleList.SelectedIndexChanged
        DisplayUsersBelongingToRole()
    End Sub

    Private Sub DisplayUsersBelongingToRole()
        ' Get the selected role
        Dim selectedRoleName As String = RoleList.SelectedValue

        ' Get the list of usernames that belong to the role
        Dim usersBelongingToRole() As String = Roles.GetUsersInRole(selectedRoleName)

        ' Bind the list of users to the GridView
        RolesUserList.DataSource = usersBelongingToRole
        RolesUserList.DataBind()
    End Sub

    Protected Sub RolesUserList_RowDeleting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewDeleteEventArgs) Handles RolesUserList.RowDeleting
        ' Get the selected role
        Dim selectedRoleName As String = RoleList.SelectedValue

        ' Reference the UserNameLabel
        Dim UserNameLabel As Label = CType(RolesUserList.Rows(e.RowIndex).FindControl("UserNameLabel"), Label)

        ' Remove the user from the role
        Roles.RemoveUserFromRole(UserNameLabel.Text, selectedRoleName)

        ' Refresh the GridView
        DisplayUsersBelongingToRole()

        ' Display a status message
        ActionStatus.Text = String.Format("User {0} was removed from role {1}.", UserNameLabel.Text, selectedRoleName)

        ' Refresh the "by user" interface
        CheckRolesForSelectedUser()
    End Sub


#End Region
End Class
