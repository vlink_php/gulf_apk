﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="video-links.aspx.cs" Inherits="admin_video_links" validateRequest="false" %>
<%@ Register src="~/controls/GridViewPager.ascx" tagname="GridViewPager" tagprefix="asp" %>
<%@ Register src="~/controls/admin-nav.ascx" tagname="anav" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  
               <asp:anav runat="server" ID="nav" />
               <p> </p> <div class="clearfix"></div>
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                           Video Tutorial Links</h3>
                    </div>
                   
                        
            <div class="panel-body"> 
            <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label" >
                                Vedio title:</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtVTitle" runat="server"  CssClass="form-control input-md"></asp:TextBox>
                            </div>
                        </div>
                   
                        
                         <div class="form-group">
                            <label class="col-md-2 control-label" >
                                Vedio Embed link:</label>
                            <div class="col-md-6">
                                <asp:TextBox ID="txtVlink" runat="server"  CssClass="form-control input-md"></asp:TextBox>
                            </div>
                        </div>
                      
                          <div class="form-group">
                                <label class="col-md-2 control-label" for="selectbasic">
                                    Status:</label>
                                <div class="col-md-4">
                                    <asp:DropDownList ID="dlStatus" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                <!--Sort Text input -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Sort:</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtSort" runat="server" MaxLength="2" Width="50" CssClass="form-control input-md"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Sort order."
                                    ControlToValidate="txtSort" ValidationGroup="new" Display="Dynamic">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                <div class="form-group">
                    <label class="col-md-2 control-label" for="singlebutton">
                    </label>
                    <div class="col-md-4">
                        <asp:Button ID="btnInsert" runat="server" Text="Submit" CssClass="btn btn-success"
                            OnClick="btnInsert_Click" />
                    </div>
                            </div>
                            </div>             

     <p> <asp:Label ID="lblMsg" runat="server" ForeColor="Red" ></asp:Label></p>
                
                <div class="col-md-12 ">
                        <hr class="divider" />
                        <div class="col-md-10">
                            <div class="form-inline">
                                Keyword: 
                              <asp:TextBox ID="txtKeyword" runat="server" MaxLength="70" Width="170" CssClass="form-control input-md"></asp:TextBox>
                                <asp:Button ID="btnSearch" runat="server" Text="Search"
                                    CssClass="btn btn-primary" ValidationGroup="search" />
                                <asp:Label ID="lblDelmsg" runat="server" CssClass="label label-danger"></asp:Label>
                                <input id="select_all" type="checkbox" />
                                Select All
                                       <p></p>
                            </div>

                        </div>
                        <div class="col-md-2 right">

                            <%--<asp:Button ID="btnDelete" runat="server" CausesValidation="false" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />
                            --%> <asp:Button ID="Button1" runat="server" CausesValidation="false" OnClick="btnDelete_Click" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />
                            
                        </div>
                    </div>
     <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
         AutoGenerateColumns="False" DataKeyNames="tutId" DataSourceID="dsVideos"  OnRowDataBound="GridView1_RowDataBound" Width="99%"
         CssClass="tablestyle" ShowFooter="False">
        <AlternatingRowStyle CssClass="altrowstyle" />
            <HeaderStyle CssClass="headerstyle" />
             <RowStyle CssClass="rowstyle" />
             <FooterStyle CssClass="footerstyle" />
         <Columns>
               <asp:TemplateField HeaderText="#">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %></ItemTemplate>
                                        
                                    </asp:TemplateField>
        
                       <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="~/images/EditContact.gif"
                 UpdateImageUrl="~/images/scr_check_10x10.gif" CancelImageUrl="~/images/scr_x_10x10.gif"
                 HeaderText="Edit" ItemStyle-HorizontalAlign="Center" />
              
         
         
             <asp:TemplateField HeaderText="#" SortExpression="pId">
                 <ItemTemplate>
                     <asp:Label ID="Label3" runat="server" Text='<%#Eval("tutid") %>'></asp:Label>
                 </ItemTemplate>
                 <%--<FooterTemplate>
                     <b>**</b>
                 </FooterTemplate>--%>
             </asp:TemplateField>
             
             <asp:TemplateField HeaderText="Sort" SortExpression="sortNews">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("sortVideo") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtSort" runat="server" Text='<%#Bind("sortVideo") %>' Width="50" MaxLength="2"></asp:TextBox>
                                </EditItemTemplate>

             </asp:TemplateField>
             
             <asp:TemplateField HeaderText="Video title" SortExpression="title" ItemStyle-HorizontalAlign="Left" FooterStyle-VerticalAlign="Middle">
                 <ItemTemplate>
                     <%#Eval("title") %>
                 </ItemTemplate>
                 <EditItemTemplate>
                     <asp:TextBox ID="txtTitle" runat="server" Text='<%#Bind("title") %>' Width="200" MaxLength="200" ></asp:TextBox>
                 </EditItemTemplate>
              <%--   <FooterTemplate>
                     <asp:TextBox ID="txtTitle" runat="server" MaxLength="200" Width="200"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter a title"
                         ControlToValidate="txtTitle" ValidationGroup="new" Display="Dynamic">*</asp:RequiredFieldValidator>
                 </FooterTemplate>--%>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Video link" SortExpression="video_link" ItemStyle-HorizontalAlign="Left" FooterStyle-VerticalAlign="Middle">
                 
                 <ItemTemplate>
                     <%#Eval("video_link")  %> 
                 </ItemTemplate>
                 <ItemStyle Width="100" />
                 <EditItemTemplate>
                     <asp:TextBox ID="txtVideoLink" runat="server" Text='<%#Bind("video_link") %>' Width="200" MaxLength="200" ></asp:TextBox>
                 </EditItemTemplate>
             <%--    <FooterTemplate>
                     <asp:TextBox ID="txtVideoLink" runat="server" MaxLength="200" Width="200"></asp:TextBox>
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter a video link"
                         ControlToValidate="txtVideoLink" ValidationGroup="new" Display="Dynamic">*</asp:RequiredFieldValidator>
                 </FooterTemplate>--%>
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Status" SortExpression="status">
                 <ItemTemplate>
                     
                     <asp:Label ID="Label2" runat="server" Text='<%#Eval("status") %>'></asp:Label>
                 </ItemTemplate>
                 <EditItemTemplate>
<asp:DropDownList ID="dlStatus" runat="server" selectedValue='<%#Bind("status") %>' >
<asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
<asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                     </asp:DropDownList>
                     
                 </EditItemTemplate>
              <%--   <FooterTemplate>
                     <asp:DropDownList ID="dlStatus" runat="server" SelectedValue='<%#Bind("status") %>'>
                         <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                         <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                     </asp:DropDownList>
                 </FooterTemplate>--%>
             </asp:TemplateField>
             
             <asp:TemplateField >
                 <ItemTemplate>
                     <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/remove.png"
                         CommandName="Delete" AlternateText="Delete record" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');"
                         ToolTip="Click to delete this record" />
                 </ItemTemplate>
              <%--   <FooterTemplate>
                     <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/add.png" OnClick="btnAddNew_Click"
                         AlternateText="Add New" ToolTip="Click to add new record" ValidationGroup="new" />
                 </FooterTemplate>--%>
             </asp:TemplateField>
         <asp:TemplateField >
                 <ItemTemplate>   
                 <asp:CheckBox runat="server" ID="chkDelete"></asp:CheckBox>
                 </ItemTemplate>
             </asp:TemplateField>
           
         </Columns>
         <PagerStyle CssClass="footer" />
         <PagerTemplate>
             <asp:GridViewPager ID="GridViewPager1" runat="server" />
         </PagerTemplate>
         <EmptyDataTemplate>
             There are currently no items in to display.
         </EmptyDataTemplate>
     </asp:GridView>

            <asp:SqlDataSource ID="dsVideos" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                SelectCommand="select * from cms_videos order by sortVideo asc"
                FilterExpression="title like '%{0}%'"
                InsertCommand="insert into cms_videos (title, video_link, status, sortVideo) values(@title, @video_link, @status, @sortVideo)"
                UpdateCommand="update cms_videos set title=@title, status=@status, video_link=@video_link, sortVideo=@sortVideo where tutid=@tutid" 
                 DeleteCommand="delete from cms_videos where tutId=@tutId">
                <UpdateParameters>
                    <asp:Parameter Name="tutId" Type="Int32" />
                    <asp:Parameter Name="title" Type="String" />
                    <asp:Parameter Name="video_link" Type="String" />
                    <asp:Parameter Name="status" Type="String" />
                    <asp:Parameter Name="sortVideo" Type="Int32"/>
                </UpdateParameters> 
                     <InsertParameters>
                <asp:ControlParameter ControlID="dlStatus" Type="String" Name="status" PropertyName="selectedValue" />
                <asp:ControlParameter ControlID="txtVTitle" Type="String" Name="title" />
                <asp:ControlParameter ControlID="txtVlink" Type="String" Name="video_link" />
               <asp:ControlParameter ControlID="txtSort" Type="Int32" Name="sortVideo" />
                </InsertParameters>
                <DeleteParameters>
                 <asp:Parameter Name="tutId" Type="Int32" />
                </DeleteParameters>
                <filterparameters>
                    <asp:ControlParameter ControlID="txtKeyword" Type="String" Name="keyword" />
                </filterparameters>
            </asp:SqlDataSource>
         </div>
         </div>
    
    <script src="../js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../js/quicksearch.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('.search_textbox').each(function (i) {
                    $(this).quicksearch("[id*=GridView1] tr:not(:has(th))", {
                        'testQuery': function (query, txt, row) {
                            return $(row).children(":eq(" + i + ")").text().toLowerCase().indexOf(query[0].toLowerCase()) != -1;
                        }
                    });
                });
            });


            $(document).ready(function () {
                $('#select_all').change(function () {
                    // alert("I am working")
                    var checkboxes = $(this).closest('form').find(':checkbox');
                    if ($(this).is(':checked')) {
                        checkboxes.prop('checked', true);
                    } else {
                        checkboxes.prop('checked', false);
                    }
                });

            });
</script>
</asp:Content>

