﻿
Imports System.Web.UI
Imports System


Partial Class Administration_ManageUsers
    Inherits System.Web.UI.Page
    ' Reference the UserNameLabel
    Dim UserNameLabel As Label

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If HttpContext.Current.User.IsInRole("admin") = False Then
            'Response.Redirect("~/admin/no-access.aspx")
            Response.Redirect("~/default.aspx")

        End If

    End Sub

    Protected Sub MemberList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles MemberList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(2).Text = "admin" Then
                e.Row.Cells(0).Enabled = False
                e.Row.Cells(0).Text = ""
                e.Row.Cells(1).Enabled = False
                e.Row.Cells(1).Text = ""
            End If
        End If
    End Sub
End Class
