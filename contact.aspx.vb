﻿
Partial Class contact
    Inherits System.Web.UI.Page
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim sc As String = txtSecurity.Text

        If Page.IsValid And (sc = Session("randomStr").ToString()) Then

            Try
                lblAdd.Text = "Your enquiry has been received, we will get back to you as soon as possible."
                sendEmail()
                clear()
            Catch ex As Exception

                lblAdd.Text = "SURPRISED! Problem with sending your query, please try again later."

            End Try
        Else
            lblAdd.Text = "Please enter the security code correctly!"
        End If

    End Sub

    Sub clear()
        txtSubject.Text = ""
        txtEmail.Text = ""
        txtName.Text = ""
        txtQuery.Text = ""

        txtSecurity.Text = ""

    End Sub
    Private Sub sendEmail()
        'Create message object and populate w/ data from form
        Dim message As System.Net.Mail.MailMessage = New System.Net.Mail.MailMessage()
        'message.From = New System.Net.Mail.MailAddress(rtxtEmail.Text.Trim())
        Dim nm, org As String
        Dim tel As String
        Dim query As String
        Dim email As String
        Dim ip As String = Request.ServerVariables("REMOTE_ADDR")

        'assign variables
        nm = txtName.Text
        query = txtQuery.Text
        email = txtEmail.Text
        org = txtSubject.Text

        message.To.Add("admin@mentalfirstaid.com")
        message.To.Add("tajuddin335@gmail.com")
        message.Subject = "Web query requested"
        message.Body = "Hi, <br/> You have a new web query." & vbCrLf & "From <hr />" & vbCrLf & "Name: " & nm & "<br /> Organisation: " & org & " <br />Email: " & email & vbCrLf & " <br />Telephone: " & tel & vbCrLf & " <br /> Query:" & query & vbCrLf & "<br /> <br /> FACET Website <br /> IP Address: " & ip
        message.IsBodyHtml = True
        'Setup SmtpClient to send email. Uses web.config settings.
        Dim smtpClient As System.Net.Mail.SmtpClient = New System.Net.Mail.SmtpClient()

        'Error handling for sending message
        Try
            smtpClient.Send(message)
            'Exception contains information on each failed receipient
        Catch recExc As System.Net.Mail.SmtpFailedRecipientsException
            For recipient = 0 To recExc.InnerExceptions.Length - 1
                Dim statusCode As System.Net.Mail.SmtpStatusCode
                'Each InnerException is an System.Net.Mail.SmtpFailed RecipientException
                statusCode = recExc.InnerExceptions(recipient).StatusCode

                If (statusCode = Net.Mail.SmtpStatusCode.MailboxBusy) Or (statusCode = Net.Mail.SmtpStatusCode.MailboxUnavailable) Then
                    'Log this to event log: recExc.InnerExceptions(recipient).FailedRecipient
                    System.Threading.Thread.Sleep(5000)
                    smtpClient.Send(message)
                Else
                    'Log error to event log.
                    'recExc.InnerExceptions(recipient).StatusCode or use statusCode
                End If

            Next
            'General SMTP execptions
        Catch smtpExc As System.Net.Mail.SmtpException
            'Log error to event log using StatusCode information in
            'smtpExc.StatusCode
        Catch ex As Exception
            'Log error to event log.
        End Try


    End Sub
End Class
