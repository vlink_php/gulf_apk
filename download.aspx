﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="download.aspx.cs" Inherits="download" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


  <div class="col-md-12 main_content center-block">
        <div class="col-md-12 content_header">
         
                    
                        <asp:Label ID="lblCountry" runat="server" ></asp:Label> >> <asp:Label ID="lblDevice" runat="server" ></asp:Label> 
                    
            Download        </div>
      
            
            <div class="col-md-4 nopadding col-md-offset-4">
            <center >
                <asp:Repeater ID="Repeater2" runat="server" DataSourceID="dsDialerType">
                
                    <ItemTemplate>
                        
                        
                            <div class="panel panel-info">
                                <div class="cholder">
                                
                                    <img src='images/<%# Eval("image") %>' alt='<%# Eval("name") %>' height="67" width="100"/>
                                    <br />
                                    <strong><%# Eval("name") %> (Apps: <%# Eval("app_id") %>)</strong> <br />
                                    <strong>
                                        <%# Eval("opc") %>
                                    </strong>
                            
                                <div class="panel-body">
                                    <asp:Label ID="lblCode" runat="server" Visible="false" Text='<%# Eval("code") %>'></asp:Label>
                                   
                                        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="dsNokia">
                                            <HeaderTemplate>
                                                <br />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="download-alt" style="background-size: 100%; width: 400px; height: 70px;">
                                                    <div class="head" style="width: 265px;">
                                                        <%# Eval("dailer_title") %>
                                                        <br />
                                                        <span class="hdlink">
                                                          <a href='<%# Eval("download_link") %>' target="_blank">   <%# Eval("download_link") %></a></span></div>
                                                    <div class="img">
                                                        <a href='<%# Eval("download_link") %>'>
                                                            <img src="images/download1.jpg" height="67" width="100"/></a></div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    
                                </div>
                                    </div>
                            </div>
                        
                        
                        <asp:SqlDataSource ID="dsNokia" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                            SelectCommand="select * from cms_dialers where status='active' and device_type=@device_type and cid=@cid and dialer_type=@dialer_type order by sort_order asc">
                            <SelectParameters>
                                <asp:QueryStringParameter QueryStringField="cid" Type="String" DefaultValue="" Name="cid" />
                                <asp:QueryStringParameter QueryStringField="device" Type="String" DefaultValue=""
                                    Name="device_type" />
                                <asp:ControlParameter ControlID="lblCode" Type="String" Name="dialer_type" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </ItemTemplate>
                </asp:Repeater>
                    </center>
          </div>               
            <asp:SqlDataSource ID="dsDialerType" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
               SelectCommand="select  * from cms_dialer_type where status='active' and country like '%'+@country+'%' order by sort_order asc" >
               <SelectParameters>
               <asp:QueryStringParameter QueryStringField="cid" Type="String" Name="cid" />
              <asp:ControlParameter ControlID="lblCountry"  Name="country" Type="String" />
               </SelectParameters>
               </asp:SqlDataSource>     
         </div>    
</asp:Content>

