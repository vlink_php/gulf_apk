﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class admin_footer_banner : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
    {
        FileUpload footer = (FileUpload)GridView1.Rows[e.RowIndex].FindControl("fuFooter");


        string fname = footer.PostedFile.FileName;
        string UpPath = Server.MapPath(@"~/images/");

        string fn = System.IO.Path.GetFileName(fname);
        
            e.NewValues["description"] = footer.PostedFile.FileName;
            //save the file
            footer.PostedFile.SaveAs(UpPath + fn);
            lblMsg.Text = "Footer updated added!";
     
    }
}