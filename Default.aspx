﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        <script type="text/javascript" charset="utf-8">
            $(window).load(function () {
                $('#sliders').flexslider({ directionNav: false });
                $('#news').flexslider({
                    directionNav: false,
                    controlNav: false,
                    slideDirection: "horizontal",   //String: Select the sliding direction, "horizontal" or "vertical"
                    slideshowSpeed: 3000,           //Integer: Set the speed of the slideshow cycling, in milliseconds
                    animationDuration: 600
                });
            });
        </script>
        <script type="text/javascript">
            function changeID(id) {
                var tabs = document.getElementsByClassName('tab')
                for (var i = 0; i < tabs.length; ++i) {
                    var item = tabs[i];
                    item.style.backgroundColor = (item.id === id) ? "#98FF98" : "#2057DD";
                    item.style.color = (item.id === id) ? "black" : "white";
                }
            }
        </script>
    <script src="js/news.js" type="text/javascript"></script>
       
    <link href="css/flexslider.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery.flexslider-min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="col-md-12">
       
                  
                  
                  <%-- <asp:Repeater ID="Repeater4" runat="server" DataSourceID="dsFooter">
                                <ItemTemplate>
                                    <img class="img-responsive" src='<%=ResolveUrl("~")%>images/<%# Eval("description") %>'
                                        alt="Greetings" /></ItemTemplate>
                            </asp:Repeater>--%>
                           
                            
        <div class="flexslider" id="sliders">
            <ul class="slides">
                <asp:Repeater ID="Repeater2" runat="server" DataSourceID="SqlDataSource1">
                    <ItemTemplate>
                        <li>
                            <img class="img-responsive " src='<%=ResolveUrl("~")%>images/<%# Eval("Slide") %>'
                                alt="Greetings" />
                        </li>
                       
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
        </div>
                        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                   SelectCommand="SELECT     id, Slide FROM         slideImg order by position ">
                                </asp:SqlDataSource>
             
        <br/>
    </div> 
    <div class="clearfix"></div>

           
               
                
                    <div class="col-md-12 nopadding">
                        <div class="col-md-6">
                            <div class="col-md-12 nopadding unews">
                                <div class="col-md-12 nopadding tcontent">
                                    <!-- Nav tabs -->
                                   
                                    
                                    <asp:Repeater ID="rptLoginPanelTab" runat="server" DataSourceID="dsLogins">
                                            <ItemTemplate>
                                            <div class="col-md-4 ntabs nopadding">
                                        <a id='tab<%# Eval("lid") %>' class="tab" href='#<%# Eval("login_id") %>' onclick="changeID(this.id)" role="tab" data-toggle="tab">
                                           <%# Eval("login_title") %>
                                        </a>
                                    </div>
                                            </ItemTemplate>
                                            </asp:Repeater>
                                    <!-- Tab panes -->
                                    <div class="col-md-2 nopadding">
                                    </div>
                                    <div class="col-md-8 nopadding">
                                        <div class="tab-content">
                                           

                                            <asp:Repeater ID="rptLoginPanel" runat="server" DataSourceID="dsLogins">
                                            <ItemTemplate>
                                            <div class='tab-pane fade <%# Eval("class") %>' id='<%# Eval("login_id") %>'>
                                                <h2>&nbsp;</h2>
                                                <div class="panel panel-info">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title"><%# Eval("login_title") %></h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <!--<form name="login" action="http://198.64.149.149:78/vsportal.php?action=users-Login" method="post" target="_blank">-->
                                                        <table class="login">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <span class="mySpan">User&nbsp;Name</span>
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" type="text" name="data[Client][login]" placeholder="User Name" autocomplete="off" readonly="" required autofocus />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <span class="mySpan">Password</span>
                                                                    </td>
                                                                    <td>
                                                                        <input class="form-control" type="password" name="data[Client][password]" placeholder="Password" readonly="" required />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>

                                                                    </td>
                                                                    <td align="left">
                                                                        <div align="left">
                                                                            <input type="hidden" name="data[Client][type]" value="0" />
                                                                            <!--<input class="btn btn-warning" type="submit" value="LOGIN" />-->
                                                                            <a class="btn btn-warning" href='<%# Eval("login_url") %>' target="_blank">LOGIN LINK</a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <!--</form>-->
                                                                                                            </div>
                                                </div>
                                            </div>
                                            </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:SqlDataSource ID="dsLogins" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                    SelectCommand="select * from cms_login_urls">
                                </asp:SqlDataSource>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-md-12 nopadding unews">
                            
                                <div class="newshead">
                                    NEWS UPDATE
                                </div>
                               
                              

                                <div id="slides">
                                <ul> 
                                <asp:Repeater ID="Repeater3" runat="server" DataSourceID="dsNews">
                                                <ItemTemplate>
                                                    <li class='slide'> <div style="font-style:normal; padding-left: 5px; padding-right: 5px;"><span class='<%# Eval("class") %>' >&nbsp;</span>  <%# Eval("description") %> </div> </li>
                                                </ItemTemplate>
                                                </asp:Repeater>
                                                </ul>
                                    <div class="btn-bar">
                                        <div id="buttons">
                                            <a id="prev" href="#">&lt;</a> <a id="next" href="#">&gt;</a>
                                        </div>
                                    </div>
                                </div>
                                
                                <!--<div class="more">
                                    <a href="index-page=rate.php.html" class="btn btn-danger">MORE RATE</a>
                                </div>-->
                                <asp:SqlDataSource ID="dsNews" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                    SelectCommand="select top 10 description, class from cms_contents where type='news' and status='active' order by sortNews asc">
                                </asp:SqlDataSource>
                            </div>
                             </div>
                        
                    </div>
               
           

            <div class="col-md-12 nopadding">
               
                                
                    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="dsCountries">
                        <ItemTemplate>
                            <div class="col-md-4">
                                <div class="cholder">
                                    <div>
                                        <a href='devices.aspx?cid=<%# Eval("cid") %>'>
                                            <img src='images/<%# Eval("image") %>' alt="Bahrain" height="67" width="100"></a></div>
                                    <div class="ctext">
                                        <%# Eval("country_name") %></div>
                                    <div class="clink">
                                        <a href='devices.aspx?cid=<%# Eval("cid") %>'>DOWNLOAD DIALER</a></div>
                                </div>
                            </div>
                            </ItemTemplate>
                    </asp:Repeater>
                   
                    <asp:SqlDataSource ID="dsCountries" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                        SelectCommand="select cid, country_name, image from cms_countries where status='active' order by sort_order asc">
                    </asp:SqlDataSource>
                
            </div>

             
           
              
            
     
</asp:Content>

