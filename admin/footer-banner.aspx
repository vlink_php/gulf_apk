﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="footer-banner.aspx.cs" Inherits="admin_footer_banner" %>
<%@ Register src="~/controls/admin-nav.ascx" tagname="anav" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <div class="col-md-12">
<asp:anav runat="server" ID="nav" />
                <p> 
     <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label></p>
                <p> <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Review errors" CssClass="vSummary" /> </p> 
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                           Site notice</h3>
                    </div>
                    <div class="panel-body">

                        <asp:GridView ID="GridView1" runat="server" DataKeyNames="conId" CssClass="tablestyle" DataSourceID="dsNotice" AutoGenerateColumns="false" OnRowUpdating="GridView1_RowUpdating">
                          <AlternatingRowStyle CssClass="altrowstyle"  />
            <HeaderStyle CssClass="headerstyle" />
             <RowStyle CssClass="rowstyle" />
             <FooterStyle CssClass="footerstyle" />
                        <Columns>
                       <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="~/images/EditContact.gif" UpdateImageUrl="~/images/scr_check_10x10.gif" CancelImageUrl="~/images/scr_x_10x10.gif" HeaderText="Edit" ItemStyle-HorizontalAlign="Center"  />
                        
                            <asp:TemplateField HeaderText="#" SortExpression="pId">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("conid") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Image Name" SortExpression="pId">
                                <ItemTemplate>
                                   <img src='../images/<%# Eval("description")%>' alt="Footer" width="505" />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:FileUpload ID="fuFooter" runat="server" />
                                 
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="fuFooter" ErrorMessage="Select a file">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                     

                <asp:SqlDataSource ID="dsNotice" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>" SelectCommand="select * from cms_contents where type='footer'" UpdateCommand="update cms_contents set description=@description where conId=@conId">
                <UpdateParameters>
                <asp:Parameter Name="conId" Type="Int32" />
                <asp:Parameter Name="description" Type="String" />
                </UpdateParameters>
                </asp:SqlDataSource>
                </div>
                </div>

                </div>
</asp:Content>

