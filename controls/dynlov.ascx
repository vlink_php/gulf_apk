﻿<%@ Control Language="C#" ClassName="dynlov" %>

<!--

Developer: Mohammed Tajuddin
Date: 27 July 2012
Description: Dynamic DropDownList for RATIS Lite parameters.
Copyright: Department of Environment and Conservation 


-->
<script runat="server">

public string dynSelected
{
get
{
    return dynDDL.SelectedValue;
        
}
set
{
    dynDDL.SelectedValue = value;
}
}

  
 public string code  
{
    get
    {
        return code;
    }
    set
    {
        dsCode.SelectParameters.Add("paramName", value);
    }


}

 public Boolean validate
 {
     get
     {
       return  validate;
     }
     set
     {
         if (value == true)
         {
             RequiredFieldValidator1.Enabled = true;
         }
     }
 }

 public string errorMsg
 {
     get
     {
         return errorMsg;
     }
     set
     {
         RequiredFieldValidator1.ErrorMessage = value;
     }

 }
 

</script>


<asp:DropDownList ID="dynDDL" runat="server" DataSourceID="dsCode" DataTextField="paramValue" 
    DataValueField="paramValue" AppendDataBoundItems="true">
    <asp:ListItem Value="" Text="%" />
</asp:DropDownList>
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ForeColor="Red" ControlToValidate="dynDDL" Enabled="false" Display="Dynamic"  >*</asp:RequiredFieldValidator>

<asp:SqlDataSource ID="dsCode" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
    SelectCommand="SELECT paramValue FROM [param] WHERE ([paramName] = @paramName) order by paramValue asc">
</asp:SqlDataSource>
                
