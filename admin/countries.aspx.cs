﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class admin_countries : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnInsert_Click(object sender, EventArgs e)
    {

        
           string fname= fuImage.PostedFile.FileName;
           string UpPath = Server.MapPath(@"~/images/");

           string fn = System.IO.Path.GetFileName(fname);
           string fcheck = Server.MapPath(@"~/images/" + fn);
            

           if (File.Exists(fcheck))
           {
               lblMsg.Text = "Sorry there is a file with the same name in the server, please rename the file";
               
           }
           else
           {
               dsCountries.InsertParameters.Add("image", fname);
               dsCountries.Insert();
               GridView2.DataBind();
               //save the file
               fuImage.PostedFile.SaveAs(UpPath + fn);
               lblMsg.Text = "Country added!";
           }
        }
    string usr = HttpContext.Current.User.Identity.Name;
    protected void GridView2_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {


        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (usr != null & usr != "admin")
            {
                e.Row.Cells[1].Enabled = false;
                e.Row.Cells[1].Text = "";

            }
        }

    }

    private int count = 0;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter adp;
    SqlDataReader rd;
    DataSet ds;
    string query;
    public void dbcon()
    {

        string connn = (System.Configuration.ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString());
        con = new SqlConnection(connn);
        con.Open();

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow Grow in GridView2.Rows)
        {
            CheckBox chselect = (CheckBox)Grow.FindControl("chkDelete");
            Label cId = (Label)Grow.FindControl("Label3");


            if (chselect.Checked)
            {

                dbcon();
                query = "delete from cms_countries where cid  ='" + cId.Text + "'";

                cmd = new SqlCommand(query, con);
                count += 1;
                cmd.ExecuteNonQuery();
                // ltlMsg.Text = "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'><i class='icon-remove'></i></button><strong> The record has been approved!</strong><br></div>";
                GridView2.DataBind();

                lblDelmsg.Text = "Total " + count + " records have been deleted!";
            }
        }
    }

}