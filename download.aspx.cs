﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class download : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
         if (Request.QueryString["cid"] == null)
        {
            Response.Redirect("~/Default.aspx");
        }
         string cname = "";

         SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString());
         string str = "select distinct (select distinct country_name from cms_countries where cid=@cid) as country, upper(device_type) device_type from cms_dialers where status='active' and device_type=@device_type and cid=@cid;";
         SqlDataReader sReader;
         string cid = Request.QueryString["cid"];
         string device = Request.QueryString["device"];

         SqlCommand cmd = new SqlCommand(str, conn);

         //bind parameter
         cmd.Parameters.AddWithValue("cid", cid);
         cmd.Parameters.AddWithValue("device_type", device);


        //open connection
         conn.Open();

        

        sReader = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

        // cmd.ExecuteNonQuery();

        while (sReader.Read())
        {
            lblCountry.Text = sReader["country"].ToString();
            lblDevice.Text = sReader["device_type"].ToString();
        }

         conn.Close();

        
    }
}