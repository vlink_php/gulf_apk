﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="devices.aspx.cs" Inherits="devices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript">

    var urlParams;
    (window.onpopstate = function () {
        var match,
        pl = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query = window.location.search.substring(1);

        urlParams = {};
        while (match = search.exec(query))
            urlParams[decode(match[1])] = decode(match[2]);
    })();
</script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:HiddenField ID="hvCid" runat="server" />
    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="dsDevice">
        <ItemTemplate>
            <div class="col-md-4">
                <a href='download.aspx?device=<%# Eval("paramValue") %>&cid=<%= hvCid.Value %>'>
           <%--<img src='images/<%# Eval("imageName") %>' alt="pc" width="280"></a>--%>
                    <img src='images/<%# Eval("imageName") %>' alt="pc" width="250" height="250" style="border-radius: 10px;"></a>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:SqlDataSource ID="dsDevice" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
        SelectCommand="select paramName, paramValue, imageName from cms_param where type='DEVICE' order by sortOrder asc">
    </asp:SqlDataSource>
</asp:Content>

