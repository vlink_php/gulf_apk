﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="admin_Default" %>

<%@ Register Src="~/controls/GridViewPager.ascx" TagName="GridViewPager" TagPrefix="asp" %>
<%@ Register Src="~/controls/admin-nav.ascx" TagName="anav" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="col-md-12">


        <p></p>
        <asp:anav runat="server" ID="nav" />
        <p></p>
        <p>
            &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Review errors" CssClass="vSummary" ValidationGroup="edit" />
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Site notice</h3>
                </div>
                <div class="panel-body">

                    <asp:GridView ID="GridView2" runat="server" DataKeyNames="conId" CssClass="tablestyle" DataSourceID="dsNotice" AutoGenerateColumns="false">
                        <AlternatingRowStyle CssClass="altrowstyle" />
                        <HeaderStyle CssClass="headerstyle" />
                        <RowStyle CssClass="rowstyle" />
                        <FooterStyle CssClass="footerstyle" />
                        <Columns>
                            <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="~/images/EditContact.gif"
                                UpdateImageUrl="~/images/scr_check_10x10.gif"
                                CancelImageUrl="~/images/scr_x_10x10.gif"
                                HeaderText="Edit"
                                ItemStyle-HorizontalAlign="Center"
                                ValidationGroup="edit" />

                            <asp:TemplateField HeaderText="#" SortExpression="pId">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("conid") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" SortExpression="pId">
                                <ItemTemplate>
                                    <%# Eval("description")%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDescription" runat="server" Text='<%#Bind("description") %>' TextMode="MultiLine" Rows="3" Width="800" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription" ErrorMessage="Enter description">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>


                    <asp:SqlDataSource ID="dsNotice" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>" SelectCommand="select * from cms_contents where type='notice'" UpdateCommand="update cms_contents set description=@description where conId=@conId">
                        <UpdateParameters>
                            <asp:Parameter Name="conId" Type="Int32" />
                            <asp:Parameter Name="description" Type="String" />
                            
                        </UpdateParameters>
                    </asp:SqlDataSource>

                </div>
            </div>
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">Update News</h3>
                </div>
                <div class="panel-body">
                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                    <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="new" />
                    <div class="form-horizontal">
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Description:</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtDescription" runat="server" MaxLength="300" Width="700" CssClass="form-control input-md"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter description."
                                    ControlToValidate="txtDescription" ValidationGroup="new" Display="Dynamic">*</asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="selectbasic">
                                Status:</label>
                            <div class="col-md-4">
                                <asp:DropDownList ID="dlStatus" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="selectbasic">
                                Style class:</label>
                            <div class="col-md-4">
                                <asp:DropDownList ID="dlClass" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="New" Value="new"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div> 
                        <!--Sort Text input -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Sort:</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtSort" runat="server" MaxLength="2" Width="50" CssClass="form-control input-md"></asp:TextBox>

                            </div>
                        </div>
                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="singlebutton"></label>
                            <div class="col-md-4">
                                <asp:Button ID="btnInsert" runat="server" Text="Add News"
                                    CssClass="btn btn-success" ValidationGroup="new" OnClick="btnInsert_Click" />
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12 ">
                        <hr class="divider" />
                        <div class="col-md-10">
                            <div class="form-inline">
                                Keyword: 
                              <asp:TextBox ID="txtKeyword" runat="server" MaxLength="70" Width="170" CssClass="form-control input-md"></asp:TextBox>
                                <asp:Button ID="btnSearch" runat="server" Text="Search"
                                    CssClass="btn btn-primary" ValidationGroup="search" />
                                <asp:Label ID="lblDelmsg" runat="server" CssClass="label label-danger"></asp:Label>
                                <input id="select_all" type="checkbox" />
                                Select All
                                       <p></p>
                            </div>

                        </div>
                        <div class="col-md-2 right">
                            <asp:Button ID="Button1" runat="server" CausesValidation="false" OnClick="btnDelete_Click" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />
                        </div>
                    </div>

                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
                        AutoGenerateColumns="False" DataKeyNames="conId" DataSourceID="dsNews" OnRowDataBound="GridView1_RowDataBound" Width="99%"
                        CssClass="tablestyle">
                        <AlternatingRowStyle CssClass="altrowstyle" />
                        <HeaderStyle CssClass="headerstyle" />
                        <RowStyle CssClass="rowstyle" />
                        <FooterStyle CssClass="footerstyle" />
                        <Columns>
                            <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="~/images/EditContact.gif"
                                UpdateImageUrl="~/images/scr_check_10x10.gif" CancelImageUrl="~/images/scr_x_10x10.gif"
                                HeaderText="Edit" ItemStyle-HorizontalAlign="Center" />
                            <asp:TemplateField HeaderText="#" SortExpression="pId">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("conid") %>'></asp:Label>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="News description" SortExpression="description" ItemStyle-HorizontalAlign="Left" FooterStyle-VerticalAlign="Middle">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("description") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtDescription" runat="server" Text='<%#Bind("description") %>' Width="500" MaxLength="300"></asp:TextBox>
                                </EditItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status" SortExpression="status">
                                <ItemTemplate>

                                    <asp:Label ID="Label2" runat="server" Text='<%#Eval("status") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="dlStatus" runat="server" SelectedValue='<%#Bind("status") %>'>
                                        <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                                    </asp:DropDownList>

                                </EditItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Class" SortExpression="class">
                                <ItemTemplate>
                                    <%#Eval("class") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="dlClass" runat="server" SelectedValue='<%#Bind("class") %>'>
                                        <asp:ListItem Text="" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="New" Value="new"></asp:ListItem>

                                    </asp:DropDownList>
                                </EditItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Sort" SortExpression="sortNews">
                                <ItemTemplate>
                                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("sortNews") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtSort" runat="server" Text='<%#Bind("sortNews") %>' Width="50" MaxLength="2"></asp:TextBox>
                                </EditItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkDelete"></asp:CheckBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="footer" />
                        <PagerTemplate>
                            <asp:GridViewPager ID="GridViewPager1" runat="server" />
                        </PagerTemplate>
                        <EmptyDataTemplate>
                            There are currently no items in to display.
                        </EmptyDataTemplate>
                    </asp:GridView>

                    <asp:SqlDataSource ID="dsNews" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                        SelectCommand="select conid, status, type, description, class, sortNews from cms_contents where type='news' order by sortNews asc"
                        FilterExpression="description like '%{0}%' or description like '%{0}%' or status like '%{0}%' or class like '%{0}%'"
                        InsertCommand="insert into cms_contents (type, description, status, class, sortNews) values('news',@description,@status, @class,@sortNews)"
                        UpdateCommand="update cms_contents set description=@description, status=@status, class=@class, sortNews=@sortNews where conid=@conid"
                        DeleteCommand="delete from cms_contents where conid=@conid">
                        <UpdateParameters>
                            <asp:Parameter Name="conId" Type="Int32" />
                            <asp:Parameter Name="description" Type="String" />
                            <asp:Parameter Name="class" Type="String" />
                            <asp:Parameter Name="status" Type="String" />
                            <asp:Parameter Name="sortNews" Type="Int32" />
                        </UpdateParameters>
                        <DeleteParameters>
                            <asp:Parameter Name="conId" Type="Int32" />
                        </DeleteParameters>
                        <InsertParameters>
                            <asp:ControlParameter ControlID="txtDescription" Type="String" Name="description" />
                            <asp:ControlParameter ControlID="dlClass" Type="String" Name="class" PropertyName="selectedValue" DefaultValue="" />
                            <asp:ControlParameter ControlID="dlStatus" Type="String" Name="status" PropertyName="selectedValue" />
                            <asp:ControlParameter ControlID="txtSort" Type="Int32" Name="sortNews" DefaultValue="0"/>
                        </InsertParameters>

                        <FilterParameters>
                            <asp:ControlParameter ControlID="txtKeyword" Type="String" Name="keyword" />
                        </FilterParameters>
                    </asp:SqlDataSource>
                </div>

            </div>
    </div>

    <script src="../js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../js/quicksearch.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('.search_textbox').each(function (i) {
                $(this).quicksearch("[id*=GridView1] tr:not(:has(th))", {
                    'testQuery': function (query, txt, row) {
                        return $(row).children(":eq(" + i + ")").text().toLowerCase().indexOf(query[0].toLowerCase()) != -1;
                    }
                });
            });
        });


        $(document).ready(function () {
            $('#select_all').change(function () {
                // alert("I am working")
                var checkboxes = $(this).closest('form').find(':checkbox');
                if ($(this).is(':checked')) {
                    checkboxes.prop('checked', true);
                } else {
                    checkboxes.prop('checked', false);
                }
            });

        });
    </script>
</asp:Content>

