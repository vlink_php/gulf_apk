﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="dialers.aspx.cs" Inherits="admin_dialers" %>

<%@ Register Src="~/controls/admin-nav.ascx" TagName="anav" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:anav runat="server" ID="nav" />
    <asp:Label ID="lblMsg" runat="server"></asp:Label>
    <p></p>
    <asp:FormView ID="FormView1" runat="server" DefaultMode="Edit" DataKeyNames="dtype" DataSourceID="dsDialers" Width="100%" OnItemUpdated="FormView1_ItemUpdated" OnItemInserting="FormView1_inserting" OnDataBound="FormView1_Databound" OnItemInserted="FormView1_ItemInserted" >
        <InsertItemTemplate>


            <p>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vSummary" ValidationGroup="new" />
            </p>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Add New Dialer</h3>
                </div>
                <div class="panel-body">
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                    <div class="form-horizontal">
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Dialer Name:</label>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtName" runat="server" CssClass="form-control input-md" Text='<%# Bind("name") %>' placeholder="Short name of the dialer name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter a dialer name" ControlToValidate="txtName" ValidationGroup="new" Display="Dynamic"></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Code:</label>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtCode" runat="server" CssClass="form-control input-md" Text='<%# Bind("code") %>' placeholder="Short name of the dialer without space"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Enter a code for the dialer" ControlToValidate="txtCode" ValidationGroup="new" Display="Dynamic"></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Country:</label>
                            <div class="col-md-6">
                                <asp:CheckBoxList ID="chkCountry" CssClass="table table-condensed" BorderStyle="None" runat="server" DataSourceID="dsCountries" DataTextField="country_name" DataValueField="country_name"
                                    RepeatColumns="4">
                                </asp:CheckBoxList>
                                <asp:SqlDataSource ID="dsCountries" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                    SelectCommand="select country_name, cid from cms_countries" />
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                App Id:</label>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtAppId" runat="server" CssClass="form-control input-md" Text='<%# Bind("app_id") %>' placeholder="App ID"></asp:TextBox>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                OPC:</label>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtOpc" runat="server" CssClass="form-control input-md" Text='<%# Bind("opc") %>' placeholder="OPC"></asp:TextBox>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Sorting Order:</label>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtSortOrder" runat="server" CssClass="form-control input-md" Text='<%# Bind("sort_order") %>' placeholder="Sort order"></asp:TextBox>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="fuImage">
                                Image:</label>
                            <div class="col-md-4">
                                <asp:FileUpload ID="fuImage" runat="server" CssClass="control-label" /><asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    runat="server" ErrorMessage="Select a file." ControlToValidate="fuImage" ValidationGroup="new" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>

                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="selectbasic">
                                Status:</label>
                            <div class="col-md-3">
                                <asp:DropDownList ID="dlStatus" runat="server" CssClass="form-control" SelectedValue='<%# Bind("status") %>'>
                                    <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="singlebutton"></label>
                            <div class="col-md-4">
                                <asp:Button ID="btnInsert" runat="server" Text="Add New" CommandName="Insert"
                                    CssClass="btn btn-primary" ValidationGroup="new"  />
                            </div>
                        </div>

                    </div>


                </div>
            </div>



        </InsertItemTemplate>
        <EditItemTemplate>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">Add New Dialer</h3>
                </div>
                <div class="panel-body">

                    <div class="form-horizontal">
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Dialer Name:</label>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtName" runat="server" CssClass="form-control input-md" Text='<%# Bind("name") %>' placeholder="Short name of the dialer name"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter a dialer name" ControlToValidate="txtName" ValidationGroup="new" Display="Dynamic"></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Code:</label>
                            <div class="col-md-5">
                                <asp:TextBox ID="txtCode" runat="server" CssClass="form-control input-md" Text='<%# Bind("code") %>' placeholder="Short name of the dialer without space"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Enter a code for the dialer" ControlToValidate="txtCode" ValidationGroup="new" Display="Dynamic"></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Country:</label>
                            <div class="col-md-6">
                                <asp:CheckBoxList ID="chkCountry" BorderColor="Gray" BorderWidth="1" CellPadding="3" CellSpacing="3" runat="server" DataSourceID="dsCountries" DataTextField="country_name" DataValueField="country_name"
                                    RepeatColumns="4">
                                </asp:CheckBoxList>
                                <asp:SqlDataSource ID="dsCountries" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                    SelectCommand="select country_name, cid from cms_countries" />
                                <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("country") %>' Visible="false"></asp:Label>
                            </div>
                        </div>

                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                App Id:</label>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtAppId" runat="server" CssClass="form-control input-md" Text='<%# Bind("app_id") %>' placeholder="App ID"></asp:TextBox>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                OPC:</label>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtOpc" runat="server" Text='<%# Bind("opc") %>' CssClass="form-control input-md" placeholder="OPC"></asp:TextBox>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="textinput">
                                Sorting Order:</label>
                            <div class="col-md-2">
                                <asp:TextBox ID="txtSortOrder" runat="server" CssClass="form-control input-md" Text='<%# Bind("sort_order") %>' placeholder="Sort order"></asp:TextBox>
                            </div>
                        </div>


                        <!-- Select Basic -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="selectbasic">
                                Status:</label>
                            <div class="col-md-3">
                                <asp:DropDownList ID="dlStatus" runat="server" CssClass="form-control" SelectedValue='<%# Bind("status") %>'>
                                    <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-2 control-label" for="singlebutton"></label>
                            <div class="col-md-4">
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" PostBackUrl="~/admin/dialers.aspx" CausesValidation="false" CssClass="btn btn-warning" />
                                <asp:Button ID="btnUpdate" runat="server" Text="Update" CommandName="Update"
                                    CssClass="btn btn-primary" ValidationGroup="new" />
                            </div>
                        </div>

                    </div>


                </div>
            </div>
        </EditItemTemplate>
    </asp:FormView>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Existing Countriesries</h3>
        </div>
        <div class="panel-body">


            <div class="col-md-12 ">
                <hr class="divider" />
                <div class="col-md-10">
                    <div class="form-inline">
                        Keyword: 
                              <asp:TextBox ID="txtKeyword" runat="server" MaxLength="70" Width="170" CssClass="form-control input-md"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" Text="Search"
                            CssClass="btn btn-primary" ValidationGroup="search" />
                        <asp:Label ID="lblDelmsg" runat="server" CssClass="label label-danger"></asp:Label>
                        <input id="select_all" type="checkbox" />
                        Select All
                                       <p></p>
                    </div>

                </div>
                <div class="col-md-2 right">

                    <%--<asp:Button ID="btnDelete" runat="server" CausesValidation="false" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />
                    --%>
                    <asp:Button ID="Button1" runat="server" CausesValidation="false" OnClick="btnDelete_Click" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />

                </div>
            </div>

            <asp:GridView ID="GridView2" runat="server" DataKeyNames="dtype" OnRowDataBound="GridView2_RowDataBound" CssClass="tablestyle" DataSourceID="dsDialersList" AutoGenerateColumns="false">
                <AlternatingRowStyle CssClass="altrowstyle" Width="99%" />
                <HeaderStyle CssClass="headerstyle" />
                <RowStyle CssClass="rowstyle" />
                <FooterStyle CssClass="footerstyle" />
                <Columns>
                    <asp:TemplateField HeaderText="#" SortExpression="dtype">
                 <ItemTemplate>
                     <asp:Label ID="Label3" runat="server" Text='<%#Eval("dtype") %>'></asp:Label>
                 </ItemTemplate>
                
             </asp:TemplateField>
                    <asp:TemplateField HeaderText="Edit">
                        <ItemTemplate>
                            <a href='dialers.aspx?did=<%#Eval("dtype") %>'>
                                <img src="../images/EditContact.gif" alt="Edit" /></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="#" SortExpression="dType">
                        <ItemTemplate>
                            <%#Eval("dtype") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%#Eval("dtype") %>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Name" SortExpression="Name">
                        <ItemTemplate>
                            <%# Eval("name")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtName" runat="server" Text='<%# Bind("name")%>' MaxLength="30" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName" ErrorMessage="Enter dialer name" Display="Dynamic">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Code" SortExpression="code">
                        <ItemTemplate>
                            <%# Eval("code")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtCode" runat="server" Text='<%# Bind("code")%>' MaxLength="30" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCode" ErrorMessage="Enter dialer name" Display="Dynamic">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Country" SortExpression="country">
                        <ItemTemplate>
                            <%# Eval("country")%>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("country")%>'></asp:Label>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="App Id" SortExpression="app_id">
                        <ItemTemplate>
                            <%# Eval("app_id")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtAppId" runat="server" Text='<%# Bind("app_id")%>' MaxLength="30" Width="30" />

                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="OPC" SortExpression="opc">
                        <ItemTemplate>
                            <%# Eval("opc")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtOPC" runat="server" Text='<%# Bind("opc")%>' MaxLength="30" Width="60" />

                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sorting" SortExpression="sort_order">
                        <ItemTemplate>
                            <%# Eval("sort_order")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtSortOrder" runat="server" Text='<%# Bind("sort_order")%>' MaxLength="10" Width="30" />

                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Image Name" SortExpression="image">
                        <ItemTemplate>
                            <img src='../images/<%# Eval("image")%>' alt="Image" class="img img-responsive" width="120" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <img src='../images/<%# Eval("image")%>' alt="Image" class="img img-responsive" width="120" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" SortExpression="status">
                        <ItemTemplate>
                            <%#Eval("status") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:DropDownList ID="dlStatus" runat="server" SelectedValue='<%#Bind("status") %>'>
                                <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/remove.png"
                                CommandName="Delete" AlternateText="Delete record" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');"
                                ToolTip="Click to delete this record" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="chkDelete"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="dsDialers" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                SelectCommand="select * from cms_dialer_type where dtype=@dtype" UpdateCommand="update cms_dialer_type set name=@name, status=@status, code=@code, app_id=@app_id, opc=@opc, sort_order=@sort_order, country=@country where dtype=@dtype"
                InsertCommand="insert into cms_dialer_type (name, status, opc, image, code, sort_order, app_id, country) values(@name,@status, @opc, @image, @code,@sort_order, @app_id, @country)" DeleteCommand="delete from cms_dialer_type where dtype=@dtype">
                <UpdateParameters>
                    <asp:Parameter Name="dtype" Type="Int32" />
                    <asp:Parameter Name="name" Type="String" />
                    <asp:Parameter Name="opc" Type="String" />
                    <asp:Parameter Name="code" Type="String" />
                    <asp:Parameter Name="sort_order" Type="Int32" />
                    <asp:Parameter Name="status" Type="String" />
                    <asp:Parameter Name="app_id" Type="String" />
                </UpdateParameters>
                <InsertParameters>
                    <asp:Parameter Name="name" Type="String" />
                    <asp:Parameter Name="opc" Type="String" />
                    <asp:Parameter Name="code" Type="String" />
                    <asp:Parameter Name="app_id" Type="String" />
                    <asp:Parameter Name="sort_order" Type="Int32" DefaultValue="0" />
                    <asp:Parameter Name="status" Type="String" />
                </InsertParameters>

                <SelectParameters>
                    <asp:QueryStringParameter Name="dtype" Type="String" QueryStringField="did" />
                </SelectParameters>
            </asp:SqlDataSource>


            <asp:SqlDataSource ID="dsDialersList" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                SelectCommand="select * from cms_dialer_type"
                FilterExpression="name like '%{0}%' or country like '%{0}%' or code like '%{0}%'"
                DeleteCommand="delete from cms_dialer_type where dtype=@dtype">
                <DeleteParameters>
                    <asp:Parameter Name="dtype" Type="Int32" />
                </DeleteParameters>

                <FilterParameters>
                    <asp:ControlParameter ControlID="txtKeyword" Type="String" Name="keyword" />
                </FilterParameters>
            </asp:SqlDataSource>

        </div>
    </div>

    <script src="../js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../js/quicksearch.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('.search_textbox').each(function (i) {
                $(this).quicksearch("[id*=GridView2] tr:not(:has(th))", {
                    'testQuery': function (query, txt, row) {
                        return $(row).children(":eq(" + i + ")").text().toLowerCase().indexOf(query[0].toLowerCase()) != -1;
                    }
                });
            });
        });


        $(document).ready(function () {
            $('#select_all').change(function () {
                // alert("I am working")
                var checkboxes = $(this).closest('form').find(':checkbox');
                if ($(this).is(':checked')) {
                    checkboxes.prop('checked', true);
                } else {
                    checkboxes.prop('checked', false);
                }
            });

        });
    </script>
</asp:Content>

