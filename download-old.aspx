﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="download-old.aspx.cs" Inherits="download" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="col-md-12 nopadding">


<div class="col-md-12 nopadding">
     <div class="col-md-4">
       <div class="dload nopadding">
        <div class="nokia">
        DIALERS FOR NOKIA
       </div>
       
           <asp:Repeater ID="Repeater1" runat="server" DataSourceID="dsNokia">
               <ItemTemplate>
                   <div class="alert alert-info info-small" role="alert">
                       <a href='<%# Eval("download_link") %>'>
                           <img src='images/<%# Eval("dialer_type") %>.png' width="32" />
                           <%# Eval("dailer_title") %>
                           <span style="color: #F9966B; font-weight: bold;">OPC -
                               <%# Eval("opc") %></span></a></div>
               </ItemTemplate>
           </asp:Repeater>
           <asp:SqlDataSource ID="dsNokia" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
               SelectCommand="select * from cms_dialers where status='active' and device_type='nokia' and cid=@cid">
               <SelectParameters>
                   <asp:QueryStringParameter QueryStringField="cid" Type="String" DefaultValue="" Name="cid" />
               </SelectParameters>
           </asp:SqlDataSource>
    </div>
    </div>
    <div class="col-md-4">
       <div class="dload nopadding">
        <div class="android">
        DIALERS FOR ANDROID
       </div>
        <asp:Repeater ID="Repeater2" runat="server" DataSourceID="dsAndroid">
               <ItemTemplate>
                   <div class="alert alert-info info-small" role="alert">
                       <a href='<%# Eval("download_link") %>'>
                           <img src='images/<%# Eval("dialer_type") %>.png' width="32" />
                           <%# Eval("dailer_title") %>
                           <span style="color: #F9966B; font-weight: bold;">OPC -
                               <%# Eval("opc") %></span></a></div>
               </ItemTemplate>
           </asp:Repeater>
           <asp:SqlDataSource ID="dsAndroid" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
               SelectCommand="select * from cms_dialers where status='active' and device_type='android' and cid=@cid">
               <SelectParameters>
                   <asp:QueryStringParameter QueryStringField="cid" Type="String" DefaultValue="" Name="cid" />
               </SelectParameters>
           </asp:SqlDataSource>
    </div>
    </div>
    <div class="col-md-4">
       <div class="dload nopadding">
        <div class="apple">
        DIALERS FOR APPLE
       </div>
        <asp:Repeater ID="Repeater3" runat="server" DataSourceID="dsApple">
               <ItemTemplate>
                   <div class="alert alert-info info-small" role="alert">
                       <a href='<%# Eval("download_link") %>'>
                           <img src='images/<%# Eval("dialer_type") %>.png' width="32" />
                           <%# Eval("dailer_title") %>
                           <span style="color: #F9966B; font-weight: bold;">OPC -
                               <%# Eval("opc") %></span></a></div>
               </ItemTemplate>
           </asp:Repeater>
           <asp:SqlDataSource ID="dsApple" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
               SelectCommand="select * from cms_dialers where status='active' and device_type='apple' and cid=@cid">
               <SelectParameters>
                   <asp:QueryStringParameter QueryStringField="cid" Type="String" DefaultValue="" Name="cid" />
               </SelectParameters>
           </asp:SqlDataSource>
    </div>
    </div>
    </div>


    <div class="col-md-12 nopadding">
     <div class="col-md-4">
       <div class="dload nopadding">
        <div class="blackberry">
        DIALERS FOR BLACKBERRY
       </div>
       <asp:Repeater ID="Repeater4" runat="server" DataSourceID="dsBlackberry">
               <ItemTemplate>
                   <div class="alert alert-info info-small" role="alert">
                       <a href='<%# Eval("download_link") %>'>
                           <img src='images/<%# Eval("dialer_type") %>.png' width="32" />
                           <%# Eval("dailer_title") %>
                           <span style="color: #F9966B; font-weight: bold;">OPC -
                               <%# Eval("opc") %></span></a></div>
               </ItemTemplate>
           </asp:Repeater>
           <asp:SqlDataSource ID="dsBlackberry" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
               SelectCommand="select * from cms_dialers where status='active' and device_type='blackberry' and cid=@cid">
               <SelectParameters>
                   <asp:QueryStringParameter QueryStringField="cid" Type="String" DefaultValue="" Name="cid" />
               </SelectParameters>
           </asp:SqlDataSource>
    </div>
    </div>
    <div class="col-md-4">
       <div class="dload nopadding">
        <div class="windows">
        DIALERS FOR WINDOWS
       </div>
        <asp:Repeater ID="Repeater5" runat="server" DataSourceID="dsWindows">
               <ItemTemplate>
                   <div class="alert alert-info info-small" role="alert">
                       <a href='<%# Eval("download_link") %>'>
                           <img src='images/<%# Eval("dialer_type") %>.png' width="32" />
                           <%# Eval("dailer_title") %>
                           <span style="color: #F9966B; font-weight: bold;">OPC -
                               <%# Eval("opc") %></span></a></div>
               </ItemTemplate>
           </asp:Repeater>
           <asp:SqlDataSource ID="dsWindows" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
               SelectCommand="select * from cms_dialers where status='active' and device_type='windows' and cid=@cid">
               <SelectParameters>
                   <asp:QueryStringParameter QueryStringField="cid" Type="String" DefaultValue="" Name="cid" />
               </SelectParameters>
           </asp:SqlDataSource>
    </div>
    </div>
    <div class="col-md-4">
       <div class="dload nopadding">
        <div class="pc">
        DIALERS FOR PC2PHONE
       </div>
        <asp:Repeater ID="Repeater6" runat="server" DataSourceID="dsPc">
               <ItemTemplate>
                   <div class="alert alert-info info-small" role="alert">
                       <a href='<%# Eval("download_link") %>'>
                           <img src='images/<%# Eval("dialer_type") %>.png' width="32" />
                           <%# Eval("dailer_title") %>
                           <span style="color: #F9966B; font-weight: bold;">OPC -
                               <%# Eval("opc") %></span></a></div>
               </ItemTemplate>
           </asp:Repeater>
           <asp:SqlDataSource ID="dsPc" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
               SelectCommand="select * from cms_dialers where status='active' and device_type='pc' and cid=@cid">
               <SelectParameters>
                   <asp:QueryStringParameter QueryStringField="cid" Type="String" DefaultValue="" Name="cid" />
               </SelectParameters>
           </asp:SqlDataSource>
    </div>
    </div>

   
    
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                  <tbody><tr>
                    <td height="60" align="center" background="images/bg1.jpg" valign="middle"><table border="0" cellpadding="0" cellspacing="0" width="90%">
                      <tbody><tr>
                            <td align="left" width="59%">MOBINOR GENERAL DIALER<br>
                          LINK: <span class="hdlink3">www.mobinor.net/gen.sis </span></td>
                        <td align="center" width="41%"><a href="#"><img src="images/download1.jpg" height="37" border="0" width="118"></a></td>
                      </tr>
                    </tbody></table></td>
                  </tr>
                </tbody></table>
   </div>
    
    </div>
</asp:Content>

