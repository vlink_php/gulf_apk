﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="download-links.aspx.cs" Inherits="admin_download_links" %>
<%@ Register src="~/controls/GridViewPager.ascx" tagname="GridViewPager" tagprefix="asp" %>

<%@ Register src="~/controls/admin-nav.ascx" tagname="anav" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <script src="../js/quicksearch.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('.search_textbox').each(function (i) {
                    $(this).quicksearch("[id*=GridView1] tr:not(:has(th))", {
                        'testQuery': function (query, txt, row) {
                            return $(row).children(":eq(" + i + ")").text().toLowerCase().indexOf(query[0].toLowerCase()) != -1;
                        }
                    });
                });
            });

            $(document).ready(function () {
                $('#select_all').change(function () {
                    // alert("I am working")
                    var checkboxes = $(this).closest('form').find(':checkbox');
                    if ($(this).is(':checked')) {
                        checkboxes.prop('checked', true);
                    } else {
                        checkboxes.prop('checked', false);
                    }
                });

            });
        </script>

 <style type="text/css" >

input.search_textbox { width:50px;}
</style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="col-md-12">

               
                   <asp:anav runat="server" ID="nav" />
                     <div class="clearfix"></div>
                <p> </p>
                <p> &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vSummary" ValidationGroup="new" />
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                           Download links</h3>
                    </div>
                    <div class="panel-body">

                    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" ></asp:Label>

                    
                        <asp:ValidationSummary ID="ValidationSummary2" runat="server" ValidationGroup="new" />
                        <div class="form-horizontal">
                           
                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="selectbasic">
                                    Country:</label>
                                <div class="col-md-4">
                                    <asp:DropDownList runat="server" ID="dlCountries" DataSourceID="dsCountries" DataTextField="country_name" CssClass="form-control"
                                        DataValueField="cid">
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsCountries" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                        SelectCommand="select country_name, cid from cms_countries" />
                                </div>
                            </div>
                             <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="selectbasic" >
                                    Device type:</label>
                                <div class="col-md-4">
                                    <asp:DropDownList runat="server" ID="dlDeviceType" CssClass="form-control" DataSourceID="dsDevices" DataTextField="paramName" DataValueField="paramValue">
                                        
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsDevices" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                        SelectCommand="select paramName, paramValue, imageName from cms_param where type='DEVICE' order by sortOrder asc" />
                                </div>
                            </div>
                            <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="selectbasic">
                                    Dialer type:</label>
                                <div class="col-md-4">
                                <asp:DropDownList runat="server" ID="dlDialerType" DataSourceID="dsDialer" DataTextField="name" DataValueField="code" CssClass="form-control"
                                        >
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsDialer" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                        SelectCommand="select name, code from cms_dialer_type" />
                                    
                                </div>
                            </div>
                             <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="textinput">
                                    Dialer title:</label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtTitle" runat="server" MaxLength="300" Width="700" CssClass="form-control input-md"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter title."
                                        ControlToValidate="txtTitle" ValidationGroup="new" Display="Dynamic">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                             <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="textinput">
                                    Sort Order:</label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtSortOrder" runat="server" MaxLength="20" Width="100" CssClass="form-control input-md"></asp:TextBox>
                                    
                                </div>
                            </div>
                             <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="textinput">
                                    Download link:</label>
                                <div class="col-md-8">
                                    <asp:TextBox ID="txtDownloadLink" runat="server" MaxLength="300" Width="700" CssClass="form-control input-md"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter download link."
                                        ControlToValidate="txtDownloadLink" ValidationGroup="new" Display="Dynamic">*</asp:RequiredFieldValidator>
                                </div>
                            </div>
                             <!-- Select Basic -->
                            <div class="form-group">
                                <label class="col-md-2 control-label" for="selectbasic">
                                    Status:</label>
                                <div class="col-md-4">
                                    <asp:DropDownList ID="dlStatus" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            
                            <!-- Button -->
                            <div class="form-group">
                              <label class="col-md-2 control-label" for="singlebutton"></label>
                              
                              <div class="col-md-4">
                                <asp:Button ID="btnInsert" runat="server" Text="Submit" 
                                      CssClass="btn btn-success" ValidationGroup="new" onclick="btnInsert_Click" />
                              &nbsp;&nbsp;&nbsp;
                              </div>
                            </div>
</div>


<!-- Button -->
                            <div class="col-md-12 ">
                            <hr class="divider" />
                            <div class="col-md-10">
                            <div class="form-inline">
                               
                            
                           Keyword: 
                              <asp:TextBox ID="txtKeyword" runat="server" MaxLength="70" Width="170" CssClass="form-control input-md"></asp:TextBox> 
                               <asp:Button ID="btnSearch" runat="server" Text="Search" 
                                      CssClass="btn btn-primary" ValidationGroup="search"  /> 
                                <asp:Label ID="lblDelmsg" runat="server" CssClass="label label-danger"></asp:Label>
<input id="select_all" type="checkbox" /> Select All
                                       <p>Label3 </p>
                                      </div>
                                     
                             </div>
                             <div class="col-md-2 right">
                                  
                                 <asp:Button ID="btnDelete" runat="server" CausesValidation="false" OnClick="btnDelete_Click" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" /> </div>
                             </div>
     
     <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True"
         AutoGenerateColumns="False" DataKeyNames="dnId" DataSourceID="dsNews"  
                         OnRowDataBound="GridView1_RowDataBound" Width="99%" OnDataBound="GridView1_DataBound"
         CssClass="tablestyle">   <%--OnDataBound="GridView1_DataBound" --%>
        <AlternatingRowStyle CssClass="altrowstyle" />
            <HeaderStyle CssClass="headerstyle" />
             <RowStyle CssClass="rowstyle" />
             <FooterStyle CssClass="footerstyle" />
         <Columns>
             <asp:CommandField ShowEditButton="true" 
                 ButtonType="Image" 
                 EditImageUrl="~/images/EditContact.gif"
                 UpdateImageUrl="~/images/scr_check_10x10.gif" 
                 CancelImageUrl="~/images/scr_x_10x10.gif"
                 ValidationGroup="edit"
                 HeaderText="Edit" 
                 ItemStyle-HorizontalAlign="Center" />

             <asp:TemplateField HeaderText="#" SortExpression="pId">
                 <ItemTemplate>
                     <asp:Label ID="Label3" runat="server" Text='<%# Eval("dnid") %>'></asp:Label>
                    
                 </ItemTemplate>
         
                 <FooterTemplate>
                     <b>**</b>
                 </FooterTemplate>
             </asp:TemplateField>
              <asp:TemplateField HeaderText="Country" SortExpression="country_name" ItemStyle-HorizontalAlign="Left" FooterStyle-VerticalAlign="Middle">
                 
                  <ItemTemplate>
                      
                     <%#Eval("country_name") %>
                     
                 </ItemTemplate>
                 <EditItemTemplate>
                 <asp:DropDownList runat="server" selectedValue='<%#Bind("cid") %>' ID="dlCountries" DataSourceID="dsCountries" DataTextField="country_name" DataValueField="cid" >
             </asp:DropDownList>
                    
                      <asp:SqlDataSource ID="dsCountries" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                SelectCommand="select country_name, cid from cms_countries" />
                    
                 </EditItemTemplate>
                 
             </asp:TemplateField>
              <asp:TemplateField HeaderText="Dialer Type" SortExpression="dialer_type" ItemStyle-HorizontalAlign="Left" FooterStyle-VerticalAlign="Middle">
                 <ItemTemplate>
                     <%#Eval("dialer_type") %>
                 </ItemTemplate>
                 <EditItemTemplate>

             <asp:DropDownList runat="server" ID="dlDialerType" DataSourceID="dsDialer" DataTextField="name" DataValueField="code"  selectedValue='<%#Bind("dialer_type") %>'
                                        >
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsDialer" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                        SelectCommand="select name, code from cms_dialer_type" />
                      <%
                          var sh = dlDialerType;
                      %>
                     
                 </EditItemTemplate>
               
             </asp:TemplateField>
              <asp:TemplateField HeaderText="Device" SortExpression="device_type" ItemStyle-HorizontalAlign="Left" FooterStyle-VerticalAlign="Middle">
                 <ItemTemplate>
                     <%#Eval("device_type") %>
                 </ItemTemplate>
                 <EditItemTemplate>
                
             <asp:DropDownList runat="server" ID="dlDeviceType"  DataSourceID="dsDevices" DataTextField="paramName" DataValueField="paramValue" selectedValue='<%#Bind("device_type") %>'>
                                        
                                    </asp:DropDownList>
                                    <asp:SqlDataSource ID="dsDevices" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                        SelectCommand="select paramName, paramValue, imageName from cms_param where type='DEVICE' order by sortOrder asc" />
                    
                 </EditItemTemplate>
                
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Dialer title" SortExpression="dailer_title" ItemStyle-HorizontalAlign="Left" FooterStyle-VerticalAlign="Middle">
                 <ItemTemplate>
                     <%#Eval("dailer_title")%>
                 </ItemTemplate>
                 <EditItemTemplate>
                     <asp:TextBox ID="txtDialerTitle" runat="server" Text='<%#Bind("dailer_title") %>' Width="110" MaxLength="100" ></asp:TextBox>
                 </EditItemTemplate>
                 
             </asp:TemplateField>
              <asp:TemplateField HeaderText="Sorting" SortExpression="sort_order" ItemStyle-HorizontalAlign="Left" FooterStyle-VerticalAlign="Middle">
                 <ItemTemplate>
                     <%#Eval("sort_order")%>
                 </ItemTemplate>
                 <EditItemTemplate>
                     <asp:TextBox ID="txtSortOrder" runat="server" Text='<%#Bind("sort_order") %>' Width="30" MaxLength="10" ></asp:TextBox>
                 </EditItemTemplate>
                 
             </asp:TemplateField>
             <asp:TemplateField HeaderText="Download link" SortExpression="download_link" ItemStyle-HorizontalAlign="Left" FooterStyle-VerticalAlign="Middle">
                 <ItemTemplate>
                     <%#Eval("download_link") %>
                 </ItemTemplate>
                 <EditItemTemplate>
                     <asp:TextBox ID="txtDownloadLink" runat="server" Text='<%#Bind("download_link") %>' Width="150" MaxLength="200" ></asp:TextBox>
                 </EditItemTemplate>
                
             </asp:TemplateField>
            
             <asp:TemplateField HeaderText="Status" SortExpression="status">
                 <ItemTemplate>
                     
                     <%#Eval("status") %>
                 </ItemTemplate>
                 <EditItemTemplate>
<asp:DropDownList ID="dlStatus" runat="server" selectedValue='<%#Bind("status") %>' >
<asp:ListItem Text="Active" Value="active" Selected="True"></asp:ListItem>
<asp:ListItem Text="Inactive" Value="inactive"></asp:ListItem>
                     </asp:DropDownList>
                     
                 </EditItemTemplate>
                 
             </asp:TemplateField>
            
             <asp:TemplateField >
                 <ItemTemplate>
                     
                 <asp:CheckBox runat="server" ID="chkDelete">
                 </asp:CheckBox>
                 </ItemTemplate>
                
             </asp:TemplateField>
         </Columns>
         <PagerStyle CssClass="footer" />
         <PagerTemplate>
             <asp:GridViewPager ID="GridViewPager1" runat="server" />
         </PagerTemplate>
         <EmptyDataTemplate>
             There are currently no items in to display.
         </EmptyDataTemplate>
     </asp:GridView>

            <asp:SqlDataSource ID="dsNews" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                SelectCommand="select dnId, d.cid, dialer_type, device_type, d.sort_order, dailer_title, opc, d.status, download_link, country_name from cms_dialers d, cms_countries c where d.cid=c.cid order by dnId desc" 
                FilterExpression="dailer_title like '%{0}%' or dialer_type like '%{0}%' or device_type like '%{0}%' or country_name like '%{0}%' or download_link like '%{0}%' or status like '%{0}%'"
                InsertCommand="insert into cms_dialers (dialer_type, device_type, dailer_title,  download_link, status, cid, sort_order) values(@dialer_type, @device_type, @dailer_title,  @download_link, @status, @cid,@sort_order)"
                UpdateCommand="update cms_dialers set dialer_type='sh', device_type=@device_type, dailer_title=@dailer_title, download_link=@download_link, sort_order=@sort_order, status=@status, cid=@cid where dnId=@dnid" 
                 DeleteCommand="delete from cms_dialers where dnid=@dnid" >
                
                <UpdateParameters>
                    <asp:Parameter Name="dnId" Type="Int32" />
                     <asp:Parameter Name="sort_order" Type="Int32" />
                    <asp:Parameter Name="dialer_type" Type="String" />
                    <asp:Parameter Name="device_type" Type="String" />
                    <asp:Parameter Name="dailer_title" Type="String" />
                    <asp:Parameter Name="status" Type="String"  />
                    <asp:Parameter Name="opc" Type="String" />
                    <asp:Parameter Name="download_link" Type="String" />
                    <asp:Parameter Name="cid" Type="String" />
                </UpdateParameters>
                <DeleteParameters>
                <asp:Parameter Name="dnId" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                <asp:ControlParameter ControlID="dlStatus" Type="String" Name="status" PropertyName="selectedValue" />
                <asp:ControlParameter ControlID="txtTitle" Type="String" Name="dailer_title" />
                <asp:ControlParameter ControlID="txtSortOrder" Type="Int32" Name="sort_order" />
                <asp:ControlParameter ControlID="txtDownloadLink" Type="String" Name="download_link" />
                 <asp:ControlParameter ControlID="dlCountries" Type="String" Name="cid" PropertyName="selectedValue" />
                  <asp:ControlParameter ControlID="dlDialerType" Type="String" Name="dialer_type" PropertyName="selectedValue" />
                   <asp:ControlParameter ControlID="dlDeviceType" Type="String" Name="device_type" PropertyName="selectedValue" />
                </InsertParameters>
                <FilterParameters>
                <asp:ControlParameter ControlID="txtKeyword" Type="String" Name="keyword" />
                </FilterParameters>
            </asp:SqlDataSource>
                    </div>

                    </div>

    </div>

    
</asp:Content>

