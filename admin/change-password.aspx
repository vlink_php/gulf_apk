﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="change-password.aspx.cs" Inherits="change_password" %>
<%@ Register src="~/controls/admin-nav.ascx" tagname="anav" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    

    <asp:anav runat="server" ID="nav" />
    
    <br />

 <asp:ValidationSummary ID="RegisterUserValidationSummary" runat="server" CssClass="alert alert-error" Width="400" ValidationGroup="ChangePassword1" 
                        />

                         <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                           <h2>Change your password</h2></h3>
                    </div>
                    <div class="panel-body">
                    <p>Enter your current and new password then  click on Change password button. </p>
                    
<asp:ChangePassword ID="ChangePassword1" CssClass="form-horizontal" runat="server" ChangePasswordButtonStyle-CssClass="btn btn-primary" CancelButtonStyle-CssClass="btn btn-inverse" CancelDestinationPageUrl="~/Default.aspx">
<CancelButtonStyle CssClass="btn btn-inverse"></CancelButtonStyle>

<ChangePasswordButtonStyle CssClass="btn btn-primary"></ChangePasswordButtonStyle>
    <ChangePasswordTemplate>
        
                    <table cellpadding="3" cellspacing="3" >
                        <tr>
                            <td align="center" colspan="2">
                                Change Your Password</td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="CurrentPasswordLabel" runat="server" 
                                    AssociatedControlID="CurrentPassword">Password:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" 
                                    ControlToValidate="CurrentPassword" ErrorMessage="Password is required." 
                                    ToolTip="Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="NewPasswordLabel" runat="server" 
                                    AssociatedControlID="NewPassword">New Password:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" 
                                    ControlToValidate="NewPassword" ErrorMessage="New Password is required." 
                                    ToolTip="New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Label ID="ConfirmNewPasswordLabel" runat="server" 
                                    AssociatedControlID="ConfirmNewPassword">Confirm New Password:</asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" 
                                    ControlToValidate="ConfirmNewPassword" 
                                    ErrorMessage="Confirm New Password is required." 
                                    ToolTip="Confirm New Password is required." ValidationGroup="ChangePassword1">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:CompareValidator ID="NewPasswordCompare" runat="server" 
                                    ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword" 
                                    Display="Dynamic" 
                                    ErrorMessage="The Confirm New Password must match the New Password entry." 
                                    ValidationGroup="ChangePassword1"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2" style="color:Red;">
                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Button ID="ChangePasswordPushButton" runat="server" 
                                    CommandName="ChangePassword" CssClass="btn btn-primary" Text="Change Password" 
                                    ValidationGroup="ChangePassword1" />
                            </td>
                            <td>
                                <asp:Button ID="CancelPushButton" runat="server" CausesValidation="False" 
                                    CommandName="Cancel" CssClass="btn btn-inverse" Text="Cancel" PostBackUrl="~/admin/Default.aspx" />
                            </td>
                        </tr>
                    </table>
     
    </ChangePasswordTemplate>
</asp:ChangePassword>

</div>

                    </div>
</asp:Content>


