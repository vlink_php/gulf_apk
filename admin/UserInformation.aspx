﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="UserInformation.aspx.vb" Inherits="Administration_UserInformation" title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div id="nifty">
<div class="container">
    <h2>User Information</h2>
    <p>
        <asp:HyperLink ID="BackLink" runat="server" CssClass="btn btn-primary" 
            NavigateUrl="ManageUsers.aspx">&lt;&lt; Back to User List</asp:HyperLink>
    </p>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    
   
    <table>
        <tr>
            <td class="tdLabel">Username:</td>
            <td><asp:Label runat="server" ID="UserNameLabel"></asp:Label></td>
        </tr>
        <tr>
            <td class="tdLabel">Approved:</td>
            <td>
                <asp:CheckBox ID="IsApproved" runat="server" AutoPostBack="true" />
            </td>
        </tr>
        <tr>
            <td class="tdLabel">Locked Out:</td>
            <td>
                <asp:Label runat="server" ID="LastLockoutDateLabel"></asp:Label>
                <br />
                <asp:Button runat="server" ID="UnlockUserButton" Text="Unlock" CssClass="button" />
            </td>
        </tr>
           
    </table>
    <p>
        <asp:Label ID="StatusMessage"  runat="server"></asp:Label>
    </p>
    <h3>Change Password</h3>
    <p>New Password: 
    <asp:TextBox ID="txtNewPassword" runat="server" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1"
        runat="server"  ControlToValidate="txtNewPassword" ValidationGroup="cp" ErrorMessage="Please enter a password">*</asp:RequiredFieldValidator>  
        <asp:Button ID="btnChangePwd" runat="server" Text="Change Password" ValidationGroup="cp" CausesValidation="true" /></p>
    
      

    <fieldset><legend><h3>User Role Management</h3></legend>    
    <p align="center">
        <asp:Label ID="ActionStatus" runat="server" CssClass="info" Visible="false" />
    </p>
    <h3>Manage Roles By User</h3>
    <p>
        <b>Select a User:</b>
        <asp:DropDownList ID="UserList" runat="server" AutoPostBack="True" 
            DataTextField="UserName" DataValueField="UserName">
        </asp:DropDownList>
    </p>
    <p>
        <asp:Repeater ID="UsersRoleList" runat="server">
            <ItemTemplate>
                <asp:CheckBox runat="server" ID="RoleCheckBox" AutoPostBack="true" Text='<%# Container.DataItem %>' OnCheckedChanged="RoleCheckBox_CheckChanged" />
                <br />
            </ItemTemplate>
        </asp:Repeater>
    </p>
    
    <h3>Manage Users By Role</h3>
    <table width="50%">
    <tr><td>Select a Role:</td><td><asp:DropDownList ID="RoleList" runat="server" AutoPostBack="true" /></td></tr>
    <tr><td colspan="2">
    <h4>Tick the box to add user to the role/s.</h4>
    <asp:GridView ID="RolesUserList" runat="server" AutoGenerateColumns="False" 
            EmptyDataText="No users belong to this role." CssClass="details">
            <Columns>
            <asp:TemplateField>
                 <ItemTemplate>
                     <%# CType(Container, GridViewRow).RowIndex + 1%>
                 </ItemTemplate>
             </asp:TemplateField>
                <asp:CommandField DeleteText="Remove" ShowDeleteButton="True" />
                <asp:TemplateField HeaderText="Users">
                    <ItemTemplate>
                        <asp:Label runat="server" id="UserNameLabel" Text='<%# Container.DataItem %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView></td></tr>
        
    </table>
       </fieldset>
       </ContentTemplate>
    </asp:UpdatePanel> 

     <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1"
        DynamicLayout="false">
        <ProgressTemplate>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/ajax-loading.gif" BackColor="Transparent" />
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
        HorizontalSide="Center" VerticalSide="Middle" TargetControlID="UpdateProgress1">
    </asp:AlwaysVisibleControlExtender>
    </div>
</div>
</asp:Content>
