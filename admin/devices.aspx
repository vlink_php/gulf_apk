﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="devices.aspx.cs" Inherits="admin_devices" %>

<%@ Register Src="~/controls/admin-nav.ascx" TagName="anav" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:anav runat="server" ID="nav" />
    <div class="clearfix"></div>

    <p>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vSummary" />
    </p>
    <div class="panel panel-warning">
        <div class="panel-heading">
            <h3 class="panel-title">Add New Device</h3>
        </div>
        <div class="panel-body">
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
            <div class="form-horizontal">
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-2 control-label" for="textinput">
                        Device Name:</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtDeviceName" runat="server" CssClass="form-control input-md" placeholder="Enter a device name"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter a device name" ControlToValidate="txtDeviceName" Display="Dynamic" ValidationGroup="new"></asp:RequiredFieldValidator>

                    </div>
                </div>
                <asp:Label ID="Label1" runat="server"></asp:Label>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-2 control-label" for="textinput">
                        Short Code:</label>
                    <div class="col-md-4">
                        <asp:TextBox ID="txtCode" runat="server" CssClass="form-control input-md" placeholder="Short code of the device"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter a dialer name" ControlToValidate="txtCode" ValidationGroup="new" Display="Dynamic"></asp:RequiredFieldValidator>

                    </div>
                </div>
                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-2 control-label" for="textinput">
                        Display Order:</label>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtOrder" runat="server" CssClass="form-control input-md" placeholder="Display order" MaxLength="4"></asp:TextBox>

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-2 control-label" for="fuImage">
                        Image:</label>
                    <div class="col-md-4">
                        <asp:FileUpload ID="fuImage" runat="server" CssClass="control-label" /><asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                            runat="server" ErrorMessage="Select a file." ControlToValidate="fuImage" ValidationGroup="new" Display="Dynamic"></asp:RequiredFieldValidator>
                    </div>
                </div>

                <!-- Button -->
                <div class="form-group">
                    <label class="col-md-2 control-label" for="singlebutton"></label>
                    <div class="col-md-4">
                        <asp:Button ID="btnInsert" runat="server" Text="Add New"
                            OnClick="btnInsert_Click" CssClass="btn btn-primary" ValidationGroup="new" />
                    </div>
                </div>

            </div>



        </div>
    </div>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Existing Countriesries</h3>
        </div>
        <div class="panel-body">

            <div class="col-md-12 ">
                <hr class="divider" />
                <div class="col-md-10">
                    <div class="form-inline">
                        Keyword: 
                              <asp:TextBox ID="txtKeyword" runat="server" MaxLength="70" Width="170" CssClass="form-control input-md"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" Text="Search"
                            CssClass="btn btn-primary" ValidationGroup="search" />
                        <asp:Label ID="lblDelmsg" runat="server" CssClass="label label-danger"></asp:Label>
                        <input id="select_all" type="checkbox" />
                        Select All
                                       <p></p>
                    </div>

                </div>
                <div class="col-md-2 right">

                   <%-- <asp:Button ID="btnDelete" runat="server" CausesValidation="false" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />
                    --%><asp:Button ID="Button1" runat="server" CausesValidation="false" OnClick="btnDelete_Click" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />
                    
                </div>
            </div>

            <asp:GridView ID="GridView2" runat="server" DataKeyNames="pid" CssClass="tablestyle" OnRowDataBound="GridView2_RowDataBound" DataSourceID="dsCountries" AutoGenerateColumns="false">
                <AlternatingRowStyle CssClass="altrowstyle" Width="99%" />
                <HeaderStyle CssClass="headerstyle" />
                <RowStyle CssClass="rowstyle" />
                <FooterStyle CssClass="footerstyle" />
                <Columns>
                    <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="~/images/EditContact.gif" UpdateImageUrl="~/images/scr_check_10x10.gif" CancelImageUrl="~/images/scr_x_10x10.gif" HeaderText="Edit" ItemStyle-HorizontalAlign="Center" ValidationGroup="edit" />

                    <%--<asp:TemplateField HeaderText="#" SortExpression="cid">
                        <ItemTemplate>
                            <%#Eval("pid") %>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <%#Eval("pid") %>
                        </EditItemTemplate>
                    </asp:TemplateField>--%>
                    
                    
                    <asp:TemplateField HeaderText="#" SortExpression="pId">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("pid") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%#Eval("pid") %>
                                </EditItemTemplate>
                            </asp:TemplateField>
                    

                    <asp:TemplateField HeaderText="Device Name" SortExpression="paramName">
                        <ItemTemplate>
                            <%# Eval("paramName")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDeviceName" runat="server" Text='<%# Bind("paramname")%>' MaxLength="30" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDeviceName" ErrorMessage="Enter device name" Display="Dynamic" ValidationGroup="edit">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Device Code" SortExpression="paramValue">
                        <ItemTemplate>
                            <%# Eval("paramValue")%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDeviceValue" runat="server" Text='<%# Bind("paramValue")%>' MaxLength="30" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDeviceValue" ErrorMessage="Enter device code" Display="Dynamic" ValidationGroup="edit">*</asp:RequiredFieldValidator>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sort Order" SortExpression="sortOrder">
                        <ItemTemplate>
                            <%#Eval("sortOrder") %>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:TextBox ID="txtSortOrder" runat="server" Text='<%# Bind("sortOrder")%>' MaxLength="3" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Image Name" SortExpression="image">
                        <ItemTemplate>
                            <img src='../images/<%# Eval("imageName")%>' alt="Image Name" class="img img-responsive" width="120" />
                        </ItemTemplate>
                        <EditItemTemplate>
                            <img src='../images/<%# Eval("imageName")%>' alt="Image Name" class="img img-responsive" width="120" />
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/remove.png"
                                CommandName="Delete" AlternateText="Delete record" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');"
                                ToolTip="Click to delete this record" />
                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="chkDelete"></asp:CheckBox>
                        </ItemTemplate>
                    </asp:TemplateField> 
                </Columns>
            </asp:GridView>


            <asp:SqlDataSource ID="dsCountries" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>" 
                SelectCommand="select * from cms_param where type='DEVICE'" 
                FilterExpression="paramName like '%{0}%' or paramValue like '%{0}%'"
                UpdateCommand="update cms_param set paramName=@paramname, paramValue=@paramValue, sortOrder=@sortOrder where pid=@pid"
                InsertCommand="insert into cms_param (paramname, paramValue, sortOrder, imageName, type) values(@paramname, @paramValue, @sortOrder, @image, 'DEVICE')" 
                DeleteCommand="delete from cms_param where pid=@pid">
                <UpdateParameters>
                    <asp:Parameter Name="pid" Type="Int32" />
                    <asp:Parameter Name="paramname" Type="String" />
                    <asp:Parameter Name="paramValue" Type="String" />
                    <asp:Parameter Name="sortOrder" Type="Int32" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:Parameter Name="pid" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtDeviceName" Name="paramname" Type="String" />
                    <asp:ControlParameter ControlID="txtCode" Name="paramValue" Type="String" />
                    <asp:ControlParameter ControlID="txtOrder" Name="sortOrder" Type="Int32" />

                </InsertParameters>
                
                <filterparameters>
                    <asp:ControlParameter ControlID="txtKeyword" Type="String" Name="keyword" />
                </filterparameters>
            </asp:SqlDataSource>
        </div>
    </div>

    <script src="../js/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="../js/quicksearch.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $('.search_textbox').each(function (i) {
                $(this).quicksearch("[id*=GridView2] tr:not(:has(th))", {
                    'testQuery': function (query, txt, row) {
                        return $(row).children(":eq(" + i + ")").text().toLowerCase().indexOf(query[0].toLowerCase()) != -1;
                    }
                });
            });
        });


        $(document).ready(function () {
            $('#select_all').change(function () {
                // alert("I am working")
                var checkboxes = $(this).closest('form').find(':checkbox');
                if ($(this).is(':checked')) {
                    checkboxes.prop('checked', true);
                } else {
                    checkboxes.prop('checked', false);
                }
            });

        });
    </script>
</asp:Content>

