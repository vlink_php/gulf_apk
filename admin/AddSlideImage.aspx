﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AddSlideImage.aspx.cs" Inherits="admin_devices" %>

<%@ Register Src="~/controls/admin-nav.ascx" TagName="anav" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:anav runat="server" ID="nav" />
    <div class="clearfix"></div>

    <p>
        &nbsp;<asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="vSummary" />
        <div class="panel panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title">Slides</h3>
            </div>
            <div class="panel-body">
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
                <div class="form-horizontal">

                    <asp:Label ID="Label1" runat="server"></asp:Label>
                    <div class="form-group">
                        <label class="col-lg-4 control-label" for="fuImage">
                            Note:Upload image size:1000px X 250px</label>

                    </div>
                    <!-- Text input-->
                    <div class="form-group">

                        <label class="col-md-2 control-label" for="fuImage">
                            Image:</label>
                        <div class="col-md-4">
                            <asp:FileUpload ID="fuImage" runat="server" CssClass="control-label" /><asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                runat="server" ErrorMessage="Select a file." ControlToValidate="fuImage" ValidationGroup="new" Display="Dynamic"></asp:RequiredFieldValidator>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="textinput">
                            Slide Order:</label>
                        <div class="col-md-2">
                            <asp:TextBox ID="txtPositoin" runat="server" CssClass="form-control input-md" placeholder="Display order"></asp:TextBox>

                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="singlebutton"></label>
                        <div class="col-md-4">
                            <asp:Button ID="btnInsert" runat="server" Text="Add New"
                                OnClick="btnInsert_Click" CssClass="btn btn-primary" ValidationGroup="new" />
                        </div>
                    </div>

                </div>



            </div>
        </div>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Slides</h3>
            </div>
            <div class="panel-body">

                <div class="col-md-12 ">
                    <hr class="divider" />
                    <div class="col-md-10">
                        <div class="form-inline">

                            <asp:Label ID="lblDelmsg" runat="server" CssClass="label label-danger"></asp:Label>
                            <input id="select_all" type="checkbox" />
                            Select All
                                       <p></p>
                        </div>

                    </div>
                    <div class="col-md-2 right">

                        <%--<asp:Button ID="btnDelete" runat="server" CausesValidation="false" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />
                        --%>
                        <asp:Button ID="Button1" runat="server" CausesValidation="false" OnClick="btnDelete_Click" Text="Delete selected" CssClass="btn btn-danger" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');" />

                    </div>
                </div>


                <asp:GridView ID="GridView2" runat="server" DataKeyNames="id"
                    CssClass="tablestyle" OnRowDataBound="GridView2_RowDataBound"
                    DataSourceID="dsCountries" AutoGenerateColumns="False">
                    <AlternatingRowStyle CssClass="altrowstyle" Width="99%" />
                    <HeaderStyle CssClass="headerstyle" />
                    <RowStyle CssClass="rowstyle" />
                    <FooterStyle CssClass="footerstyle" />
                    <Columns>

                        <asp:TemplateField HeaderText="#" SortExpression="id">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("id") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="~/images/EditContact.gif" UpdateImageUrl="~/images/scr_check_10x10.gif" CancelImageUrl="~/images/scr_x_10x10.gif" HeaderText="Edit" ItemStyle-HorizontalAlign="Center" ValidationGroup="edit" />

                        <%--  <asp:TemplateField HeaderText="Position" SortExpression="cid">
                                <ItemTemplate>
                                    <%#Eval("position") %>
                                </ItemTemplate>
                                <EditItemTemplate>
                                 <%#Eval("position") %>
                                </EditItemTemplate>
                            </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="position" SortExpression="position">
                            <ItemTemplate>
                                <%# Eval("position")%>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtDeviceName" runat="server" Text='<%# Bind("position")%>' MaxLength="30" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDeviceName" ErrorMessage="position" Display="Dynamic" ValidationGroup="edit">*</asp:RequiredFieldValidator>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Image Name" SortExpression="image">
                            <ItemTemplate>
                                <img src='../images/<%# Eval("Slide")%>' alt="Image Name" class="img img-responsive" width="120" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <img src='../images/<%# Eval("Slide")%>' alt="Image Name" class="img img-responsive" width="120" />
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/remove.png"
                                    CommandName="Delete" AlternateText="Delete record" OnClientClick="return confirm('This will delete the record. Are you sure you want to do this?');"
                                    ToolTip="Click to delete this record" />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkDelete"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>



                <asp:SqlDataSource ID="dsCountries" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                    SelectCommand="select * from slideImg order by position desc "
                    InsertCommand="insert into slideImg ( Slide, position) values( @Slide, @position)" DeleteCommand="delete from slideImg where id=@id" UpdateCommand="update slideImg  set position = @position  where id = @id ">
                    <UpdateParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                        <asp:Parameter Name="position" Type="Int32" />

                    </UpdateParameters>

                    <InsertParameters>

                        <asp:ControlParameter ControlID="txtPositoin" Name="position" Type="Int32" />

                    </InsertParameters>

                    <DeleteParameters>
                        <asp:Parameter Name="id" Type="Int32" />
                    </DeleteParameters>



                </asp:SqlDataSource>
            </div>
        </div>

        <script src="../js/jquery-1.10.2.min.js" type="text/javascript"></script>
        <script src="../js/quicksearch.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(function () {
                $('.search_textbox').each(function (i) {
                    $(this).quicksearch("[id*=GridView1] tr:not(:has(th))", {
                        'testQuery': function (query, txt, row) {
                            return $(row).children(":eq(" + i + ")").text().toLowerCase().indexOf(query[0].toLowerCase()) != -1;
                        }
                    });
                });
            });


            $(document).ready(function () {
                $('#select_all').change(function () {
                    // alert("I am working")
                    var checkboxes = $(this).closest('form').find(':checkbox');
                    if ($(this).is(':checked')) {
                        checkboxes.prop('checked', true);
                    } else {
                        checkboxes.prop('checked', false);
                    }
                });

            });
        </script>
</asp:Content>

