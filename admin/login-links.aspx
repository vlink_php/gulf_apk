﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="login-links.aspx.cs" Inherits="admin_login_links" %>
<%@ Register src="~/controls/admin-nav.ascx" tagname="anav" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div class="col-md-12">

                
                   <asp:anav runat="server" ID="nav" />
                <p> </p>
                <p> <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Review errors" CssClass="vSummary" ValidationGroup="new" /></p>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                           Site notice</h3>
                    </div>
                    <div class="panel-body">

                        <asp:GridView ID="GridView2" runat="server" DataKeyNames="lid" CssClass="tablestyle" DataSourceID="dsLogins" AutoGenerateColumns="false">
                          <AlternatingRowStyle CssClass="altrowstyle" Width="99%" />
            <HeaderStyle CssClass="headerstyle" />
             <RowStyle CssClass="rowstyle" />
             <FooterStyle CssClass="footerstyle" />
                        <Columns>
                       <asp:CommandField ShowEditButton="true" ButtonType="Image" EditImageUrl="~/images/EditContact.gif" UpdateImageUrl="~/images/scr_check_10x10.gif" CancelImageUrl="~/images/scr_x_10x10.gif" HeaderText="Edit" ItemStyle-HorizontalAlign="Center"  />
                        
                            <asp:TemplateField HeaderText="#" SortExpression="lid">
                                <ItemTemplate>
                                    <asp:Label ID="Label3" runat="server" Text='<%#Eval("lid") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Login ID" SortExpression="login_id">
                                <ItemTemplate>
                                    <%# Eval("login_id")%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                 <%# Eval("login_id")%>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description" SortExpression="login_title">
                                <ItemTemplate>
                                    <%# Eval("login_title")%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                 <asp:TextBox ID="txtLoginTitle" runat="server" Text='<%#Bind("login_title") %>' MaxLength="30"  Width="100%" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtLoginTitle" ErrorMessage="Enter description" Display="Dynamic">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Login URL" SortExpression="pId">
                                <ItemTemplate>
                                    <%# Eval("login_url")%>
                                </ItemTemplate>
                                <EditItemTemplate>
                                 <asp:TextBox ID="txtLoginUrl" runat="server" Text='<%#Bind("login_url") %>'  Width="100%" MaxLength="120" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLoginUrl" ErrorMessage="Enter description" Display="Dynamic">*</asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                     

                <asp:SqlDataSource ID="dsLogins" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>" SelectCommand="select * from cms_login_urls" UpdateCommand="update cms_login_urls set login_url=@login_url, login_title=@login_title where lid=@lid">
                <UpdateParameters>
                <asp:Parameter Name="lid" Type="Int32" />
                <asp:Parameter Name="login_url" Type="String" />
                <asp:Parameter Name="login_title" Type="String" />
                </UpdateParameters>
                </asp:SqlDataSource>
            
                    </div>
                </div>

               
                </div>
</asp:Content>

