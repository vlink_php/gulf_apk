﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class admin_devices : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnInsert_Click(object sender, EventArgs e)
    {


        string fname = fuImage.PostedFile.FileName;
        string UpPath = Server.MapPath(@"~/images/");

        string fn = System.IO.Path.GetFileName(fname);
        string fcheck = Server.MapPath(@"~/images/" + fn);


        
            dsCountries.InsertParameters.Add("image", fname);
            dsCountries.Insert();
            GridView2.DataBind();
            //save the file
            fuImage.PostedFile.SaveAs(UpPath + fn);
            lblMsg.Text = "Device added!";
        
    }


    protected void GridView2_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
    {
        GridView gridView = (GridView)sender;

        if (gridView.SortExpression.Length > 0)
        {
            int cellIndex = -1;
            foreach (DataControlField field in gridView.Columns)
            {
                if (field.SortExpression == gridView.SortExpression)
                {
                    cellIndex = gridView.Columns.IndexOf(field);
                    break; // TODO: might not be correct. Was : Exit For
                }
            }

            if (cellIndex > -1)
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    //  this is a header row,
                    //  set the sort style
                    e.Row.Cells[cellIndex].CssClass += (gridView.SortDirection == SortDirection.Ascending ? " sortascheader" : " sortdescheader");
                }
                else if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //  this is an alternating row
                    e.Row.Cells[cellIndex].CssClass += (e.Row.RowIndex % 2 == 0 ? " sortaltrow" : "sortrow");


                }
            }
        }


        string usr = HttpContext.Current.User.Identity.Name;

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (usr != null & usr != "admin")
            {
                e.Row.Cells[6].Enabled = false;
                e.Row.Cells[6].Text = "";

            }
        }
    }

    private int count = 0;
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter adp;
    SqlDataReader rd;
    DataSet ds;
    string query;
    public void dbcon()
    {

        string connn = (System.Configuration.ConfigurationManager.ConnectionStrings["LocalSqlServer"].ToString());
        con = new SqlConnection(connn);
        con.Open();

    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow Grow in GridView2.Rows)
        {
            CheckBox chselect = (CheckBox)Grow.FindControl("chkDelete");
            Label pid = (Label)Grow.FindControl("Label3");


            if (chselect.Checked)
            {

                dbcon();
                query = "delete from cms_param where pid  ='" + pid.Text + "'";

                cmd = new SqlCommand(query, con);
                count += 1;
                cmd.ExecuteNonQuery();
                // ltlMsg.Text = "<div class='alert alert-info'><button type='button' class='close' data-dismiss='alert'><i class='icon-remove'></i></button><strong> The record has been approved!</strong><br></div>";
                GridView2.DataBind();

                lblDelmsg.Text = "Total " + count + " records have been deleted!";
            }
        }
    }
}