﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="tutorial.aspx.cs" Inherits="tutorial" validateRequest="false"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

<style type="text/css">

.video-container {
	position:relative;
	padding-bottom:56.25%;
	padding-top:30px;
	height:0;
	overflow:hidden;
}

.video-container iframe, .video-container object, .video-container embed {
	position:absolute;
	top:0;
	left:0;
	width:100%;
	height:100%;
}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<div class="col-md-12 content_header">
            Tutorial
        </div>
        <p></p>
        <div class="clearfix"></div>
        <div class="col-md-12 nopadding">
<div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                           Video tutorials</h3>
                    </div>
                    <div class="panel-body col-md-8">

                    <p>You can download our video tutorials here.</p>
                        <asp:Repeater ID="Repeater3" runat="server" DataSourceID="dsNews">
                            <ItemTemplate>
                                   <div>
                                        <h4  class="label label-primary">
                                            <%# Eval("title") %>
                                        </h4>
                                    </div><br/>
                                <div class="video-container">
                                 
                                 <%--  <iframe width="250px" height="150px" src='<%# Eval("video_link") %>' frameborder="0" ></iframe> --%>
                                  <%--   <iframe width="640" height="360" src="https://www.youtube.com/embed/vC8LbvYk6es" frameborder="0" allowfullscreen></iframe>--%>
                                  <div ><%# Eval("video_link")%></div>
                                </div>
                                
                                <br />
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:SqlDataSource ID="dsNews" runat="server" ConnectionString="<%$ ConnectionStrings:LocalSqlServer %>"
                                    SelectCommand="select * from cms_videos where status='active' order by sortVideo asc">
                                </asp:SqlDataSource>

                                </div>

                                </div>

                                 </div>
</asp:Content>

