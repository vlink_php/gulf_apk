﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" %>

<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<h3>The Mental First Aid Checklist and Reports </h3>
<p> This Website and Checklist were developed to help as many people as possible to understand their Mental Health and Substance Abuse Problems as early as possible. Also to empower them to help themselves, and to help them to work better with their Doctors and Psychs. Getting in early is more than half the battle in beating Mental Health and Addiction Problems. It stops problems from growing, getting more and more complicated and chronic, and more and more difficult to fix. 
</p>
<p><strong>  People with Mental Health and Addiction Problems never have only 1 Problem – they always have 2, 3, 4 or more Mental Distress and Malfunction Problems e.g. Anxiety + Insomnia + Bipolar Disorder.<span style="color:Blue">Also Mental Health Medicines nearly always help 2,3 or more different Mental Health Problems e.g. Antidepressants mostly also help Anxiety + Sleep and may help ADHD.
 </span> </strong></p> 
	<p> The first goal in getting Healthy and Addiction Free is usually to find which medicines will help the greatest number of your problems the most.</p> 
    <p> Medicines kick-start your recovery and get you feeling, functioning and motivated enough to get the Information, Healthier Diet, Exercise, Sunlight, Fresh Air and Counseling/Therapy you need and to make the Life changes you need to make so that the whole of the rest of your life is better.
</p>

<h4>How This System Helps You</h4>

<p> <b>Firstly </b> when you complete this Checklist the Program instantly adds up all of your Individual Answer Scores to get your Total Score. It then puts all your Scores in your Report, with your Total Score in a graph so you can see clearly all of your Problems, which are the most important, and what progress you are making. </p>
<p> 
	A Total Score around 150 is in the normal range. A Total Score of 300 or 400 indicates several serious problems. A person with a Total Score above 500 has many serious problems, is in danger, and needs urgent very careful assessment and help. That is usually very difficult to find and often involves going onto a long waiting list. While waiting you suffer and the problems get worse. The immediate availability of this Program 24/7 every day of the year helps to reduce problems caused by waiting. 
</p>

<p><b> Secondly</b> the Program checks the predicted Score Reduction each of 16 appropriate medicines should give with each of your Problems. It then shows the 5 of those medicines that are predicted to reduce your Total Score most and what your Total Score should fall to with each of those 5 medicines.
 (See the 16 medicines at <a href="http://www.MentalHealthMedicines.com">  www.MentalHealthMedicines.com</a> and <a href="http://www.DetoxMedications.com">  www.DetoxMedications.com</a>) </p>


 <p><b>  Thirdly</b> your Doctor or you then usually choose the medicine that is predicted to lower your Total Score most. However if you know from past experience that that medicine disagrees with you, you choose a different one. You may also instead choose one of the other recommended medicines if 
it worked very well for you in the past, or worked very well for close relatives who you share many Genes with e.g. parents, siblings or children. 
You need also at this point to be reading websites and watching You Tube Programs about Diet, Exercise and the Mental and Substance Abuse problems you have features of e.g. those listed at <a href="http://www.MentalFirstAid.com"> </a>. 
</p>
<p> 
<b> Fourthly</b> if the Program predicts that when you take the 1st medicine you choose your Total Score is still going to be above 250, the Program tests the other 15 medicines to see which 5, if added to the first medicine, would further reduce your Total score most. Your Doctor or you then usually choose as a 2nd medicine one of those 5 that further reduces your Total Score most. 
</p><p>
	<b> Fifthly</b> if with those 2 medicines your Total Score is still predicted to be above 250 the Program then goes through the same process with the other 14 medicines to allow your Doctor or you to choose a 3rd medicine if you wish. If there is no Addiction it is probably best to wait a few days or weeks to see what the first 2 medicines do before deciding whether to select a 3rd medicine. However with Alcoholism and Addictions that require a Detoxing Medicine, 2 Mental Health Medicines should usually be taken from Day 1 with the Detox medicine. 
 </p>

 <p class="well">Most medicines are having most of their effect by the 3rd day, but antidepressants usually take a week to start to have a significant effect and 2 to 4 weeks to be fully effective. If you are not improving by the 4th day increase the size of the doses, except for antidepressants, where increases should only be made every 8th day as needed.	
	</p>
	<p> <b> Sixthly, </b>if you are not doing well enough the Program will help you to know whether you need an alternative or an additional medicine. 
</p>

<p> The Program analyses the Scores in your follow-up Checklist Reports to see how the actual reductions a medicine is giving you compare with what they were predicted to give you. If when you have been on full doses of a medicine for at least 7 days the actual reduction is less than 60% of what was predicted, the Program may recommend that 
you wean off that medicine and give you a choice of 5 alternative medicines. Don’t make this change if you are satisfied with your progress.  </p>
	<p>But if the chosen medicines have been at full doses for at least 7 days and are giving 60% + of their predicted effect, but your Total Score is still above 200, the Program will recommend another group of 5 medicines for you to choose an additional medicine from, to get your Score down to 150.
 </p>

<p> <b>  Finally </b>when your Total Score has mostly been around 150 for 2 or 3 months and none of your Individual Answer Scores has been above 20 the Program will recommend gradual reductions of the medicines which help those problems that are most improved. 
 By that time Counseling/Therapy, Information/Understanding and Lifestyle/Life Changes should have permanently improved many of your Distresses and Malfunctions and your need for medicine will be reduced. 
 But never make sudden big reductions e.g. never just run out of a medicine and suddenly stop it. Always keep some of each medicine on hand and if any problems ever get worse again and your Total Score rises above 200 again or an Individual Answer Score rises above 20 go back up to the last dose that was working well for another few weeks or months.
</p>

<p>
It is always a tragedy when Mental Health Distresses and Malfunctions are allowed to grow and grow and become more and more complicated, chronic and difficult to fix. People should be able to get started in the middle of the night, on the weekend, at Xmas or New Year, every day of the year, 24/7, 
whenever they are troubled and motivated to find answers and do whatever needs to be done.</p>
<p> 	Why do we so often make it harder for Troubled People at critical times to get helpful Mental Health Information and Medicines than it is to get Alcohol or to deal with Street Drug Pushers? Some medicines are addictive and dangerous e.g. Methadone, Suboxone, Xanax and sometimes Ritalin and Dexamphetamine, but we no longer need to use those medicines.
 There is now a complete range of very effective non-addictive medicines for every Problem that are far safer than Alcohol or Street Drugs. Shouldn’t we be deregulating some of these to make them easier to get than Alcohol and Street Drugs? Wouldn’t that help more people than decriminalizing the use of Drugs?   
	</p>
    <p>To maximize their profits the Street Drug Industry never bothers with anything that isn’t addictive – the more addictive the better as far as they are concerned. Why do we still have Laws and Rules and Regulations that push Troubled People in crisis into the clutches of these ruthless criminals?  
 </p>
</asp:Content>

